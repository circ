// Code adapted from Stetic which is Copyright (c) 2004 Novell, Inc

using Gtk;
using System;

namespace Circ.Frontend.GtkSharp
{
	public class TabWidget: HBox
	{
		Label tabLabel;
		Image hl;
		
		public TabWidget(string tabName, Notebook nb, int index)
		{
			tabLabel = new Label(tabName);
			hl = new Gtk.Image (Gdk.Pixbuf.LoadFromResource("chat.png"));
			
			this.PackStart (hl, true, true, 0);
			this.PackStart (tabLabel, true, true, 3);
			Button b = new Button (new Gtk.Image ("gtk-close", IconSize.Menu));
			b.Relief = Gtk.ReliefStyle.None;
			b.WidthRequest = b.HeightRequest = 24;
			
			b.Clicked += delegate (object s, EventArgs a) {
				nb.RemovePage(index);
			};
				
			this.PackStart (b, false, false, 0);
			//this.ShowAll();		
		}
		
		public bool IsHl {
			get {
				return hl.Visible;
			} set {
				if (value)
					hl.Show();
				else
					hl.Hide();
			}
		}
	}
}
