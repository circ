#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Frontend;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	[Mono.Addins.Extension("/Circ/Frontends", Id="Gtk")]
	public class GtkFrontend: IFrontend
	{
		static IFactory facto = new Factory();
		static IMainControl mainCtrl;
		MainWindow mainWindow;
		bool inited = false;
		
		// Static ctor to initialize Gtk first of all
		static GtkFrontend()
		{
			Gtk.Application.Init();
		}
		
		public void StartPresentation(IMainControl ctrl)
		{
			Mono.Unix.Catalog.Init("circ", "/usr/share/locale");
			mainWindow = new MainWindow(ctrl);
			mainCtrl = ctrl;
			inited = true;
			Gtk.Application.Run();
		}
		
		public void StopPresentation()
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				Gtk.Application.Quit();
			});
		}
		
		public void AddDiscussion(IChannelPanel panel)
		{
			Gtk.Application.Invoke( delegate (object s, EventArgs e) {
				ChannelPanel temp = (ChannelPanel)panel;
				temp.ServerPanel.AddChildDiscussion(temp);
				temp.ServerPanel.Pages.CurrentPage = temp.ServerPanel.Pages.NPages - 1;
			});
		}
		
		public void AddServer(IServerPanel panel)
		{
			Gtk.Application.Invoke( delegate {
				ServerPanel temp = (ServerPanel)panel;
				TabWidget lbl = new TabWidget(temp.ServerName, mainWindow.ServerContainer, mainWindow.ServerContainer.NPages);
				lbl.ShowAll();
				mainWindow.ServerContainer.AppendPage(temp, lbl);
				temp.ShowAll();
				mainWindow.ServerContainer.CurrentPage = mainWindow.ServerContainer.NPages - 1;
			});
		}
		
		public void ShowErrorMessage(string message)
		{
			Gtk.Application.Invoke(delegate {
				MessageDialog mess = new MessageDialog(mainWindow, DialogFlags.DestroyWithParent, MessageType.Error, ButtonsType.Close,
						                                       Mono.Unix.Catalog.GetString(message));
				mess.Run();
				mess.Destroy();
			});
		}
		
		public IFactory ToolkitFactory {
			get {
				return facto;
			}
		}
		
		public string Name {
			get {
				return "Gtk";
			}
		}
		
		public bool IsInitialized {
			get {
				return inited;
			}
		}
		
		internal static IMainControl MainControl {
			get {
				return mainCtrl;
			}
		}
	}
}
