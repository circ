#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Frontend;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	public partial class MainWindow: Gtk.Window
	{
		IMainControl ctrl;
		
		internal static ChannelPanel CurrentChannelPanel = null;
		internal static ServerPanel  CurrentServerPanel  = null;
		
		public MainWindow (IMainControl ctrl): base (Gtk.WindowType.Toplevel)
		{
			this.ctrl = ctrl;
			
			Build ();
			
			this.DeleteEvent += OnDeleteEvent;
			chatEntry.Activated += ChatEntryActivated;
			chatEntry.KeyPressEvent += ChatEntryTab;
			chatEntry.KeyReleaseEvent += SwitchingShorcut;
			serverContainer.SwitchPage += UpdateNickname;
			//this.WindowStateEvent += delegate { this.UrgencyHint = false; };
			this.chatEntry.ReceivesDefault = true;
			
			this.serverContainer.SwitchPage += delegate(object sender, SwitchPageArgs e) {
				CurrentServerPanel = (ServerPanel)this.serverContainer.GetNthPage((int)e.PageNum);
				CurrentChannelPanel = CurrentServerPanel.Pages.CurrentPageWidget as ChannelPanel;
			};
			
			this.evtBox.KeyReleaseEvent += SwitchingShorcut;
			//this.evtBox.GdkWindow.
		}
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Gtk.Application.Invoke(delegate(object s, EventArgs e) {
				Gtk.Application.Quit ();
				a.RetVal = true;
			});
		}
		
		protected void OnDeleteEvt(object sender, EventArgs e)
		{
			Gtk.Application.Quit ();	
		}

		protected virtual void ConnectionActivatedHandler(object sender, System.EventArgs e)
		{
			//TODO: Retrieve server list from the Option class
			ConnectionDialog connDialog = new ConnectionDialog(ctrl, new string[] { "EpikNet" });
			connDialog.ShowAll();
		}

		protected virtual void About_Clicked(object sender, System.EventArgs e)
		{
			AboutDialog dlg = new AboutDialog();
			dlg.Name = "Circ";
			dlg.Comments = "Circ is an IRC client written in C# using the Mono Plateform. It has been designed from the ground up to be very extensible : you can choose different IRC engines (backends), use different interface (frontends) and extend Circ with plugins. It use the excellent Mono.Addins for addins loading and management. Currently Circ is shipped with an homemade IRC library as the main backend and with a GTK# frontend as the main interface. D-Bus integration is provided with plugins.";
			dlg.Authors = new string[] { "Jérémie LAVAL" };
			dlg.Icon = Gdk.Pixbuf.LoadFromResource("circ_small.png");
			dlg.Version = "0.1";
			dlg.Logo = Gdk.Pixbuf.LoadFromResource("circ.png");
			dlg.License = "Circ is licensed under the LGPL terms";
			dlg.WrapLicense = true;
			dlg.Website = "http://netherilshade.free.fr/circ/";
			dlg.WebsiteLabel = "Official site";
			dlg.Modal = true;
			dlg.Run();
			dlg.Destroy();
		}
		
		protected void OnAddinsManager(object sender, EventArgs e)
		{
			Mono.Addins.Gui.AddinManagerWindow.Run(this);
		}
		
		/*Widget CurrentPanelShown {
			get {
				Widget temp;
				((ServerPanel)this.evtBox.Child).CurrentPageWidget;
			}
		}
		
		PanelType CurrentPanelType {
			get {
				return (CurrentPanelShown is ServerPanel) ? PanelType.Server : PanelType.Channel;
			}
		}*/
		
		// HACK: too much ugly; replace it with the MainControl facilities provided with the Shop or something
		void ChatEntryActivated(object sender, EventArgs e) {
			if (chatEntry.Text.Length >= 2 && chatEntry.Text[0] == '/') {
				object s = null;
				if (CurrentChannelPanel != null)
					s = CurrentChannelPanel.ChannelControl.Backend;
				if (s == null && CurrentServerPanel != null)
					s = CurrentServerPanel.ConnectionControl.Backend;
				/*try {
					if (((ServerPanel)this.serverContainer.CurrentPageWidget).CurrentPage == 0)
						s = ((ServerPanel)this.serverContainer.CurrentPageWidget).ConnectionControl.Backend;
					else
						s = ((ChannelPanel)((ServerPanel)this.serverContainer.CurrentPageWidget).CurrentPageWidget).ChannelControl.Backend.ParentConnection;
				} catch {}*/
				ctrl.CommandEntered(s, chatEntry.Text.Substring(1));
				chatEntry.Text = string.Empty;
			} else if (CurrentChannelPanel != null) {
				// Assume it's on a DiscussionContainer
				ChannelPanel temp = CurrentChannelPanel;
				temp.SendMessage(chatEntry.Text);
				temp.AddSelfMessage(nickLabel.Text, chatEntry.Text);
				chatEntry.Text = string.Empty;
			}
		}
				
		void ChatEntryTab(object sender, KeyPressEventArgs e) {
			if (e.Event.Key != Gdk.Key.Tab)
				return;
			
			if (!(this.serverContainer.CurrentPageWidget is ServerPanel) || ((ServerPanel)this.serverContainer.CurrentPageWidget).Pages.CurrentPage == 0)
				return;
			
			ChannelPanel temp = (ChannelPanel)((ServerPanel)this.serverContainer.CurrentPageWidget).Pages.CurrentPageWidget;
			temp.DoAutocompletation(sender, e);
		}
		
		void SwitchingShorcut(object sender, KeyReleaseEventArgs e) {
			if (!((e.Event.State & Gdk.ModifierType.SuperMask) > 0))
				return;
			
			if ((e.Event.State & Gdk.ModifierType.ControlMask) > 0) {
				switch (e.Event.Key) {
				case Gdk.Key.Right:
					this.serverContainer.NextPage();
					break;
				case Gdk.Key.Left:
					this.serverContainer.PrevPage();
					break;
				}
			} else {
				switch (e.Event.Key) {
				case Gdk.Key.Right:
					CurrentServerPanel.Pages.NextPage();
					break;
				case Gdk.Key.Left:
					CurrentServerPanel.Pages.PrevPage();
					break;
				}
			}
			System.Threading.Thread.Sleep(200);
		}
		
		// HACK: Backend info shouldn't be available like this
		internal void UpdateNickname(object sender, SwitchPageArgs e)
		{
			if (CurrentServerPanel != null)
				nickLabel.Text = CurrentServerPanel.Nickname + " : ";
		}
	}
	
	/*public enum PanelType {
		Server = 1,
		Channel
	}*/
}