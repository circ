#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Frontend;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	public partial class ServerPanel : Gtk.Bin
	{
		IConnectionControl ctrl;
		MessagesPanel messages;
		TextBuffer    buffer;
		
		public ServerPanel(IConnectionControl ctrl): base()
		{
			this.ctrl = ctrl;
			Gtk.Application.Invoke( delegate {
				this.messages = new MessagesPanel();
				this.buffer = messages.Buffer;
				
				this.Build();
				this.textWindow.Add(messages);
				this.channelsNb.SwitchPage += PageSwitched;
				
			});
			MainWindow.CurrentServerPanel = this;
		}
		
		internal Notebook Pages {
			get {
				return this.channelsNb;
			}
		}
		
		void PageSwitched(object sender, SwitchPageArgs e)
		{
			MainWindow.CurrentChannelPanel = this.channelsNb.GetNthPage((int)e.PageNum) as ChannelPanel;
		}
		
		/*public int CurrentPage {
			get {
				return this.channelsNb.CurrentPage;
			}
			set {
				this.channelsNb.CurrentPage = value;
			}
		}
		
		public Widget CurrentPageWidget {
			get {
				return this.channelsNb.CurrentPageWidget;
			}
		}
		
		public Widget GetMenuWidget(Widget widg)
		{
			return this.channelsNb.GetMenuLabel(widg);
		}
		
		public int NPages {
			get {
				return this.channelsNb.NPages;
			}
		}
		
		public void NextPage()
		{
			this.channelsNb.NextPage();
		}*/
	}
}
