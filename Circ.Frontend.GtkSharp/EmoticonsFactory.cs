#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Gtk;
using Gdk;

namespace Circ.Frontend.GtkSharp
{
	public static class EmoticonsFactory
	{
		delegate Pixbuf Worker();
		// Thinking about memoization
		public static Pixbuf GetEmoticonFromSmiley(string smiley)
		{
			Dictionary<string, Pixbuf> smileys = new Dictionary<string, Pixbuf>();
			
			Worker worker = delegate {
				Pixbuf temp;
				if (smileys.TryGetValue(smiley, out temp))
					return temp;
				
				switch (smiley) {
					case ":)":
						temp = Pixbuf.LoadFromResource("smile.png");
						smileys.Add(":)", temp);
						break;
					case ":D":
						temp = Pixbuf.LoadFromResource("wink.png");
						smileys.Add(":D", temp);
						break;
				}
				
				return temp;
			};
			
			return worker();
		}
	}
}
