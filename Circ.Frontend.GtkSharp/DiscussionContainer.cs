#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Gtk;


namespace Circ.Frontend.GtkSharp
{
	public partial class MainWindow
	{
		int numberOfDiscussions;
		int numberOfServers;
		
		/*public void AddDiscussion(string channelName, IMessagesPanel messages, IUsersPanel users)
		{
			HBox box = new HBox(false, 2);
			
			ScrolledWindow win1 = new ScrolledWindow();
			win1.HscrollbarPolicy = PolicyType.Never;
			win1.VscrollbarPolicy = PolicyType.Always;
			win1.Add((MessagesPanel)messages);
			
			ScrolledWindow win2 = new ScrolledWindow();
			win2.Add((UsersPanel)users);
			
			box.Add(win1);
			box.Add(win2);
			
			numberOfDiscussions++;
			
			Gtk.Application.Invoke( delegate (object s, EventArgs e) {
					this.discussionContainer.AppendPage(box, new Label(channelName));
					win1.ShowAll();
					win2.ShowAll();
				} );
		}
		
		public void AddServer(string serverName, IServerPanel server)
		{
			ScrolledWindow win1 = new ScrolledWindow();
			win1.HscrollbarPolicy = PolicyType.Never;
			win1.VscrollbarPolicy = PolicyType.Always;
			win1.Add((MessagesPanel)server);
			Label lbl = new Label(serverName);
			
			numberOfServers++;
			
			Logger.Debug("Updating the View with a new Server");
			Gtk.Application.Invoke( delegate (object s, EventArgs e) {
					this.discussionContainer.AppendPage(win1, lbl);
					win1.ShowAll();
					lbl.Show();
				} );
		}*/
		
		public Notebook ServerContainer {
			get {
				return serverContainer;
			}
		}
		
		public int NumberOfDiscussions {
			get {
				return numberOfDiscussions;	
			}
		}
		
		public int NumberOfServers {
			get {
				return numberOfServers;	
			}
		}
	}
}