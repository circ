#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Frontend;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	public partial class ChannelPanel : Gtk.Bin
	{
		IChannelControl ctrl;
		ServerPanel     servPanel;
		MessagesPanel   messages;
		UsersPanel      users;
		TextBuffer      buffer;
		
		public ChannelPanel(IServerPanel servPanel, IChannelControl ctrl)
		{
			
			this.ctrl      = ctrl;
			this.servPanel = (ServerPanel) servPanel;
			
			Gtk.Application.Invoke( delegate {
				this.messages  = new MessagesPanel();
				this.users     = new UsersPanel();
				this.buffer    = messages.Buffer;
				this.Build();
				this.textWindow.Add(messages);
				this.userWindow.Add(users);
				
				this.ShowAll();
			});
		}
	}
}
