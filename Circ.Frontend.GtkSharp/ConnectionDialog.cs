#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Gtk;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	public partial class ConnectionDialog : Gtk.Dialog
	{
		IMainControl ctrl;
		IList<string> selectedServers = new List<string>();
		
		public ConnectionDialog(IMainControl ctrl, string[] servers)
		{
			this.ctrl = ctrl;
			
			this.Build();
			ListStore list = new ListStore(typeof(string));
			this.serverList.HeadersVisible = false;
			this.serverList.Model = list;
			this.serverList.AppendColumn("Servers", new CellRendererText(), "text", 0);
			foreach(string s in servers)
				list.AppendValues(s);
			this.Modal = false;
			this.ShowAll();
		}
		
		public void OnProcessConnections(object sender, EventArgs e)
		{
			// TODO : connect to each selected server
		}
	}
}
