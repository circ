#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Gtk;
using Circ.Frontend;

namespace Circ.Frontend.GtkSharp
{
	public class UsersPanel: TreeView
	{
		List<string> usersList = new List<string>();
		bool usersChanged = false;
		
		public UsersPanel(): base()
		{
			ListStore list = new ListStore(typeof(string));
			this.HeadersVisible = false;
			this.Model = list;
			this.AppendColumn("Users", new CellRendererText(), "text", 0);
		}
		
		public void AddUser(string user)
		{
			this.usersList.Add(user);
			Gtk.Application.Invoke( delegate {
				((ListStore)this.Model).AppendValues(user);
			});
			usersChanged = true;
		}
		
		public void AddUser(string[] users)
		{
			Gtk.Application.Invoke( delegate {
				foreach (string s in users) {
					this.usersList.Add(s);
					((ListStore)this.Model).AppendValues(s);
				}
			});
			usersChanged = true;
		}
		
		public void RemoveUser(string user)
		{
			this.usersList.Remove(user);
			
			Gtk.Application.Invoke( delegate(object ss, EventArgs e) {
				((ListStore)this.Model).Clear();
				foreach (string s in usersList)
					((ListStore)this.Model).AppendValues(s);
			});
			usersChanged = true;
		}
		
		/*public string FindNickFromIncomplete(string partialNick)
		{
			if (usersChanged) {
				BuildTree();
				usersChanged = false;
			}
			
		}*/
		
		public string[] Users {
			get {
				return usersList.ToArray();
			}
		}
	}
}
