#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Lib;
using Gtk;
using Gdk;

namespace Circ.Frontend.GtkSharp
{

	public class MessagesPanel: TextView
	{
		static TextTagTable  txtTags;
		TextBuffer    buffer;
		TextMark      endMark;
		
		Gdk.Color background;
		
		public MessagesPanel(): base()
		{
			this.Editable = false;
			//this.DoubleBuffered = true;
			this.WrapMode = Gtk.WrapMode.Word;
			this.CursorVisible = false;
			this.LeftMargin = 3;
			this.RightMargin = 3;
			this.CanFocus = false;
			
			background = new Gdk.Color ();
			Gdk.Color.Parse (Options.GetStringConfig("background"), ref background);
			this.ModifyBg (StateType.Normal, background);
			this.ModifyBase (StateType.Normal, background);
			
			RgbColor textColor = LibContrast.GetBestColorFromBackground(new RgbColor((byte)(background.Red/257),
			                                                                          (byte)(background.Green/257), (byte)(background.Blue/257)), ColorPalette.Aqua);
			this.ModifyText(StateType.Normal, new Gdk.Color(textColor.Red, textColor.Green, textColor.Blue));
			this.ModifyFont(Pango.FontDescription.FromString(Options.GetStringConfig("font")));
			
			if (txtTags == null)
				InitTextTags();
			
			this.buffer = new TextBuffer(txtTags);
			this.Buffer = buffer;
			this.endMark = buffer.CreateMark("end", buffer.EndIter, false);
			//this.buffer.InsertText += InsertionHandler;
		}
		
		private void InitTextTags()
		{
			txtTags = new TextTagTable();
			RgbColor backgroundColor = new RgbColor((byte)(background.Red/257), (byte)(background.Green/257), (byte)(background.Blue/257));
			
			// Instanciation
			TextTag message = new TextTag("message");
			TextTag author  = new TextTag("author");
			TextTag hl      = new TextTag("hl");
			TextTag itsme   = new TextTag("itsme");
			TextTag notice  = new TextTag("notice");
			TextTag time    = new TextTag("time");
			
			
			// Tuning (follow Tango color scheme)
			RgbColor hlColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)ColorPalette.Parse(typeof(ColorPalette), Options.GetStringConfig("hl")));
			hl.ForegroundGdk = new Gdk.Color(hlColor.Red, hlColor.Green, hlColor.Blue);
			hl.PixelsBelowLines = 3;
			
			RgbColor messageColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)Enum.Parse(typeof(ColorPalette), Options.GetStringConfig("author")));
			message.ForegroundGdk = new Color(messageColor.Red, messageColor.Green, messageColor.Blue);
			message.PixelsBelowLines = 2;
			
			// This Enum.Parse() method is really really crappy
			RgbColor authorColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)Enum.Parse(typeof(ColorPalette), Options.GetStringConfig("author")));
			author.ForegroundGdk = new Color(authorColor.Red, authorColor.Green, authorColor.Blue);
			author.Weight = Pango.Weight.Bold;
			
			RgbColor meColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)ColorPalette.Parse(typeof(ColorPalette), Options.GetStringConfig("self")));
			itsme.ForegroundGdk = new Color(meColor.Red, meColor.Green, meColor.Blue);
			
			RgbColor tiColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)ColorPalette.Parse(typeof(ColorPalette), Options.GetStringConfig("date")));
			time.ForegroundGdk = new Color(tiColor.Red, tiColor.Green, tiColor.Blue);
			
			RgbColor noticeColor = LibContrast.GetBestColorFromBackground(backgroundColor, (ColorPalette)ColorPalette.Parse(typeof(ColorPalette), Options.GetStringConfig("notice")));
			notice.ForegroundGdk =  new Color(noticeColor.Red, noticeColor.Green, noticeColor.Blue);
			
			txtTags.Add(message);
			txtTags.Add(author);
			txtTags.Add(hl);
			txtTags.Add(notice);
			txtTags.Add(time);
			txtTags.Add(itsme);
		}
		
		internal void ScrollToEnd()
		{
			this.ScrollMarkOnscreen(endMark);
		}
	}
}
