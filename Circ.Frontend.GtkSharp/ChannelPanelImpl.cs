#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Controller;
using Circ.Frontend;
using Circ.Lib;
using System.Text.RegularExpressions;

namespace Circ.Frontend.GtkSharp
{
	public partial class ChannelPanel: IChannelPanel
	{		
		bool emoticon, url;
		TransformationPool<TextView> pool = new TransformationPool<TextView>();
		static Regex urlRegex = new Regex(@"(?#WebOrIP)((?#protocol)((http|https):\/\/)?(?#subDomain)(([a-zA-Z0-9]+\.(?#domain)[a-zA-Z0-9\-]+(?#TLD)(\.[a-zA-Z]+){1,2})|(?#IPAddress)((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])))+(?#Port)(:[1-9][0-9]*)?)+(?#Path)((\/((?#dirOrFileName)[a-zA-Z0-9_\-\%\~\+]+)?)*)?(?#extension)(\.([a-zA-Z0-9_]+))?(?#parameters)(\?([a-zA-Z0-9_\-]+\=[a-z-A-Z0-9_\-\%\~\+]+)?(?#additionalParameters)(\&([a-zA-Z0-9_\-]+\=[a-z-A-Z0-9_\-\%\~\+]+)?)*)?", RegexOptions.Compiled);
		TabWidget tab;
		
		public void SendMessage(string message)
		{
			ctrl.Backend.SendMessage(message);
		}
		
		public void AddNewMessage(DateTime timestamp, string author, string mess, MessageFlags flags)
		{
			Gtk.Application.Invoke( delegate {
				TextIter end = buffer.EndIter;
				/*MatchCollections matches = urlRegex.Matches(mess);
				foreach (Match match in matches) {
					match.
				}*/	
				string message = mess + Environment.NewLine;
				
				buffer.InsertWithTagsByName(ref end, FormatDate(ref timestamp), "time");
				buffer.InsertWithTagsByName(ref end, author + " :  ", "author");
				if ((flags & MessageFlags.Hl) > 0) {
					buffer.InsertWithTagsByName(ref end, message, "hl");
					RenderHl();
				}
				else
					buffer.InsertWithTagsByName(ref end, message, "message");
				messages.ScrollToEnd();
			});
		}
		
		public void AddSelfMessage(string whoami, string message)
		{
			Gtk.Application.Invoke( delegate {
				TextIter end = buffer.EndIter;
				string mess = message + Environment.NewLine;
				
				DateTime timestamp = DateTime.Now;
				buffer.InsertWithTagsByName(ref end, FormatDate(ref timestamp), "time");
				buffer.InsertWithTagsByName(ref end, whoami + mess, "itsme");
				
				messages.ScrollToEnd();
			});
		}
		
		string FormatDate(ref DateTime timestamp)
		{
			return '[' + timestamp.ToString("T") + "] ";
		}
		
		void InsertUrl(string url)
		{
			TextIter iter = this.messages.Buffer.EndIter;
			TextChildAnchor temp = this.messages.Buffer.CreateChildAnchor(ref iter);
			//Sexy.UrlLabel urlLabel = new Sexy.UrlLabel();
			//urlLabel.Markup = url;
			//this.messages.AddChildAtAnchor(urlLabel, temp);
		}
		
		void RenderHl()
		{
			if (tab != null)
				tab.IsHl = true;			
			
			((Window)this.Toplevel).UrgencyHint = true;
			GLib.Timeout.Add(2000, delegate {
				Window win = ((Window)this.Toplevel);
				if (!win.UrgencyHint)
					return false;
				
				if (win.GdkWindow.State != Gdk.WindowState.Withdrawn && win.IsActive && this == MainWindow.CurrentChannelPanel) {
					//System.Console.WriteLine("Current window state" + win.GdkWindow.State.ToString());
					win.UrgencyHint = false;
					if (tab != null)
						tab.IsHl = false;
					return false;
				}
				return true;
			});
		}
		/*string FormatMessage(string timestamp, string author, string message)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder(timestamp.Length +
			                                                             author.Length + message.Length + 10)
			sb.Append('[');
			sb.Append(timestamp);
			sb.Append(']');
			sb.Append("  ");
			sb.Append(author);
			sb.Append(':');
			sb.Append("  ");
			sb.AppendLine(message);
			
			return sb.ToString();
		}*/
		
		public void AddNewNotice(string notice)
		{
			Gtk.Application.Invoke( delegate(object sen, EventArgs ee) {
					TextIter end = buffer.EndIter;
					buffer.InsertWithTagsByName(ref end, '*' + notice + Environment.NewLine,
					                           "notice");
					messages.ScrollToEnd();
				});
		}
		
		public void ChangeTopic(string topic, string author, DateTime timestamp)
		{
			Gtk.Application.Invoke(delegate {
				System.Text.StringBuilder sb = new System.Text.StringBuilder(topic);
				sb.Replace('\n', ' ');
				sb.Replace('\r', ' ');
				sb.Replace(Environment.NewLine, " ");
				this.topicEntry.Text = sb.ToString();
			});
		}
		
		public void AddUser(string[] users)
		{
			this.users.AddUser(users);
		}
		
		public void AddUser(string user)
		{
			this.AddNewNotice(user + " joined the channel");
			this.users.AddUser(user);
		}
		
		public void RemoveUser(string user, string quitMessage)
		{
			this.AddNewNotice(user + " has left channel : " + quitMessage);
			this.users.RemoveUser(user);
		}
		
		public bool UseEmoticons {
			get {
				return emoticon;
			}
			set {
				emoticon = value;	
			}
		}
		
		public bool UseUrlHighlight {
			get {
				return url;
			}
			set {
				url = value;	
			}
		}
		
		
		internal Circ.Frontend.GtkSharp.TabWidget Tab {
			get {
				return tab;
			}
			set {
				tab = value;
			}
		}
		
		public void DoAutocompletation(object sender, KeyPressEventArgs e)
		{
			if (e.Event.Key != Gdk.Key.Tab)
				return;
			
			Entry temp = (Entry) sender;
			// TODO: build it using the Nick Tree
			foreach (string user in users.Users) {
				if (user.Length > temp.Text.Length && (user.StartsWith(temp.Text, StringComparison.Ordinal) ||
				                                       user.Equals(temp.Text, StringComparison.Ordinal)))
					temp.Text = user + ": ";
			}
			temp.Position = -1;
			e.RetVal = true;
		}
		
		internal string ChannelName {
			get {
				return ctrl.Backend.Name;
			}
		}
		
		internal ServerPanel ServerPanel {
			get {
				return servPanel;
			}
		}
		
		internal IChannelControl ChannelControl {
			get {
				return ctrl;
			}
		}
	}
}
