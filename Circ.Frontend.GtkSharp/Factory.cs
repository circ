#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Frontend;
using Circ.Controller;

namespace Circ.Frontend.GtkSharp
{
	public class Factory: Circ.Frontend.IFactory
	{
		public IChannelPanel GetChannelPanel(IServerPanel servPanel, IChannelControl ctrl)
		{
			return new ChannelPanel(servPanel, ctrl);	
		}

		
		public IServerPanel GetServerPanel(IConnectionControl ctrl)
		{
			return new ServerPanel(ctrl);
		}
	}
}
