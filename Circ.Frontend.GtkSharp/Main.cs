// project created on 13/03/2007 at 18:52
using System;
using Gtk;

namespace Circ.Backend.GtkSharp
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}