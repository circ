#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using System.Collections.Generic;

namespace Circ.Frontend.GtkSharp
{
	public class GtkText
	{
		public GtkText(string text)
		{
			this.text = text;
		}
		
		public string text;
		public readonly Queue<TextTransformation> textTrans = new Queue<TextTransformation>();
	}
	
	public struct TextTransformation
	{
		public readonly int startIndex;
		public readonly int endIndex;
		
		public readonly TextTag[]       tags;
		public readonly Gdk.Pixbuf     image;
		
		public TextTransformation(int start, int end, TextTag[] tags)
		{
			this.startIndex = start;
			this.endIndex = end;
			this.tags = tags;
			image = null;
		}
		
		public TextTransformation(int start, int end, TextTag tag): this(start, end, new TextTag[] { tag })
		{
		}
		
		public TextTransformation(int start, int end, Gdk.Pixbuf img)
		{
			this.startIndex = start;
			this.endIndex   = end;
			this.image = img;
			
			tags = null;
		}
	}
}
