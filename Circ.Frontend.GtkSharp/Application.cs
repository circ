
using System;

namespace Circ.Frontend.GtkSharp
{
	
	
	public class Application: Circ.Frontend.IApplication
	{
		public void QuitLoop()
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				Gtk.Application.Quit();
			});
		}
	}
}
