#region License
/* Circ.Frontend.GtkSharp : GTK# frontend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Gtk;
using Circ.Controller;
using Circ.Frontend;

namespace Circ.Frontend.GtkSharp
{
	public partial class ServerPanel: IServerPanel
	{
		
		public void JoinChannel(string chan)
		{
			ctrl.Backend.JoinChannel(chan);
		}
		
		public void AddNewMessage(string notice)
		{
			Gtk.Application.Invoke( delegate(object sen, EventArgs ee) {
				TextIter end = buffer.EndIter;
				buffer.Insert(ref end, notice + Environment.NewLine);
				messages.ScrollToEnd();
				HlTab();
			});
		}
		
		public void ChangeNickname(string newNick)
		{
			((MainWindow)((Widget)this.Parent).Parent).UpdateNickname(null, null);
		}
		
		private void HlTab()
		{
			
		}
		
		internal void AddChildDiscussion(ChannelPanel panel)
		{
			TabWidget lbl = new TabWidget(panel.ChannelName, channelsNb, channelsNb.NPages);
			panel.Tab = lbl;
			
			lbl.ShowAll();
			
			this.channelsNb.AppendPage(panel, lbl);
			panel.ShowAll();
			lbl.IsHl = false;
		}
		
		internal string ServerName {
			get {
				return ctrl.Backend.Info.Server;
			}
		}
		
		internal string Nickname {
			get {
				return ctrl.Backend.Nick;
			}
			set {
				ctrl.Backend.Nick = value;
			}
		}
		
		internal IConnectionControl ConnectionControl {
			get {
				return ctrl;
			}
		}
	}
}
