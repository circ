#region License
/* Circ plugin to alter qui message to the Posix apocalypse (it's a joke !)
 * Copyright (C) 2007  LAVAL J�r�mie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using Circ.Plugins;
using Circ.Backend;
using Circ;


[Mono.Addins.Extension("/Circ/Plugins", Id="ApocalypsePlugin")]
public class ApocalypsePlugin: Plugin {
	static readonly DateTime apocaDate = new DateTime(2039, 01, 19, 3, 14, 7, 0);	

	/// <summary>
	/// Ask the names of the authors of the plugin
	/// </summary>
	/// <return>The names of the authors</return>
	public override string[] Authors { get { return new string[] { "J�r�mie LAVAL"�}; }}
	
	/// <summary>
	/// Ask the name of the plugin
	/// </summary>
	/// <return>The name of the plugin</return>
	public override string   Name { get { return "Apocalypse Quit Message"; } }
	
	/// <summary>
	/// Ask the description of the plugin
	/// </summary>
	/// <return>The description of the plugin</return>
	public override string   Description { get { return "Just ask Cygal"; } }
	
	/// <summary>
	/// Ask the version in textual form of the plugin 
	/// </summary>
	/// <return>the version of the plugin</return>
	public override string   Version { get { return "0.0.1"; } }
	
	/// <summary>
	/// This method is used by the plugin to allocate the ressource it might need
	/// </summary>
	/// <return>Return true if the operation was successful, false if a problem happened</return>
	/// <remarks>If the method return false, the Dispose method is called and the plugin isn't added to the factory</remarks>
	public override bool InitializePlugin() 
	{
		IrcConnection.ConnectionCreated += RegisterQuitMessageWithNewConnection;
		
		return true;
	}
	
	static void RegisterQuitMessageWithNewConnection(object sender, EventArgs e)
	{
		IrcConnection temp = (IrcConnection)sender;
		temp.Disconnecting += AlterQuitMessage;
	}
	
	static void AlterQuitMessage(object sender, EventArgs e)
	{
		IrcConnection temp = (IrcConnection)sender;
		TimeSpan period = apocaDate - DateTime.Now;
		string str = string.Format("{0} years, {1} days, {2} hours, {3} minutes, {4} seconds before the end of the Unix period", (period.Days / 365).ToString(), (period.Days % 365).ToString(), period.Hours.ToString(), period.Minutes.ToString(), period.Seconds.ToString());
		temp.Info.QuitMessage = "There is only " + str;
	}
	
	protected override void Dispose(bool managedRes)
	{
		Logger.Debug("Plugin disposed : " + this.ToString());
	}
}