// MyClass.cs
//
//  Copyright (C) 2007 [name of author]
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
//
//
// project created on 23/07/2007 at 20:14
using System;
using System.Text;
using Circ;
using Circ.Lib;
using Circ.Plugins;
using Circ.Backend;
using Circ.Controller;
using NDesk.DBus;

[Mono.Addins.Extension("/Circ/Plugins", Id="BansheePlugin")]
public class BansheePlugin: Plugin
{
	string cmd = Options.GetStringConfig("command");
	string str = Options.GetStringConfig("pattern");
	StringBuilder sb = new StringBuilder(50);
	
	IBansheeTitleInfo banshee = Bus.Session.GetObject<IBansheeTitleInfo>("org.gnome.Banshee",
		                                                             new ObjectPath("/org/gnome/Banshee/Player"));
	
	public override string Name {
		get { return "Banshee Playing"; }
	}
	
	public override string[] Authors {
		get { return new string[] {"LAVAL Jérémie"}; }
	}
	
	public override string Description {
		get { return "This plugin show with a command what Banhee is currently playing"; }
	}
	
	public override string Version {
		get { return "0.0.1"; }
	}
	
	protected override void Dispose (bool managedRes)
	{
		Logger.Debug("Plugin disposed : " + this.ToString());
	}

	public override bool InitializePlugin ()
	{
		if (string.IsNullOrEmpty(cmd) || string.IsNullOrEmpty(str))
			return false;
		
		CommandProcesser.Instance.RegisterCommand(cmd, DoAction);
		return true;
	}
	
	void DoAction(object sender, string[] args) {
		IrcChannel chan = sender as IrcChannel;
		if (chan == null)
			return;
		
		if (banshee == null) {
			banshee = Bus.Session.GetObject<IBansheeTitleInfo>("org.gnome.Banshee",
		                                                             new ObjectPath("/org/gnome/Banshee/Player"));
			if (banshee == null)
				return;
		}
			
		
		// Clean
		sb.Remove(0, sb.Length);
		sb.Append(str);
		sb.Replace("%title", banshee.GetPlayingTitle());
		sb.Replace("%artist", banshee.GetPlayingArtist());
		sb.Replace("%album", banshee.GetPlayingAlbum());
		
		chan.SendMessage(sb.ToString());
		
		// HACK : Really refractor the API of the CommandProcesser : both presentation and model object should be passed
		try {
			Shop.MainControl.GetConnectionControl(chan.ParentConnection.Info.Server).GetChannelControl(chan.Name).Frontend.
				AddNewMessage(DateTime.Now, "BansheePlugin", sb.ToString(), Circ.Frontend.MessageFlags.Self);
		} catch {}
	}
	
	[Interface("org.gnome.Banshee.Core")]
	public interface IBansheeTitleInfo
	{
		string GetPlayingTitle();
		string GetPlayingAlbum();
		string GetPlayingArtist();
	}
}