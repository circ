// MyClass.cs
//
//  Copyright (C) 2007 [name of author]
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
//
//
// project created on 23/07/2007 at 18:34

using System;
using Circ.Plugins;
using Circ.Backend;
using Circ;
using Circ.Controller;

[Mono.Addins.Extension("/Circ/Plugins", Id="AutoconnectPlugin")]
public class AutoconnectPlugin: Plugin
{
	const char servSeparator = '|';	
	
	public override string Name {
		get { return "AutoConnect"; }
	}
	
	public override string[] Authors {
		get { return new string[] {"LAVAL Jérémie"}; }
	}
	
	public override string Description {
		get { return "This plugin connect automagically Circ to predefined severs"; }
	}
	
	public override string Version {
		get { return "0.0.1"; }
	}
	
	protected override void Dispose (bool managedRes)
	{
		Logger.Debug("Plugin disposed : " + this.ToString());
	}

	public override bool InitializePlugin ()
	{
		ConnectionInfo[] cis = RetrieveConnectionInfo();
		
		if (cis == null)
			return false;
		
		if (!Options.GetBooleanConfig("use-command")) {
			foreach (ConnectionInfo ci in cis) {
				ConnectionFactory.Factory.RequestConnection(ci);
				System.Threading.Thread.Sleep(70);
			}
		} else {
			CommandProcesser.Instance.RegisterCommand(Options.GetStringConfig("command"),
			                                                          delegate {
				foreach (ConnectionInfo ci in cis) {
					ConnectionFactory.Factory.RequestConnection(ci);
					System.Threading.Thread.Sleep(70);
				}
			});
		}
		
		return true;
	}
	
	ConnectionInfo[] RetrieveConnectionInfo()
	{
		string serv;
		string nick;
		
		if (string.IsNullOrEmpty(serv = Options.GetStringConfig("servers")) || 
		    string.IsNullOrEmpty(nick = Options.GetStringConfig("nickname")))
			return null;
		
		string[] servs = serv.Split(servSeparator);
		ConnectionInfo[] cis = new ConnectionInfo[servs.Length];
		
		for (int i = 0; i < cis.Length; i++) {
			cis[i] = ConnectionInfo.GetDefault(servs[i], nick);
		}
		
		return cis;
	}
}