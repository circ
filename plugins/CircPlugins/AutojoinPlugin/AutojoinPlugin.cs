// AutojoinPlugin.cs
//
//  Copyright (C) 2007 LAVAL Jérémie
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
//
//

using System;
using Circ.Plugins;
using Circ.Backend;
using Circ;

[Mono.Addins.Extension("/Circ/Plugins", Id="AutojoinPlugin")]
public class AutojoinPlugin: Plugin
{
	const char chanSeparator = '|'; 	
	
	public override string Name {
		get { return "AutoJoin"; }
	}
	
	
	public override string[] Authors {
		get { return new string[] {"LAVAL Jérémie"}; }
	}
	
	public override string Description {
		get { return "Automatically join specified channels when identified to known servers"; }
	}
	
	public override string Version {
		get { return "0.0.1"; }
	}

	
	protected override void Dispose (bool managedRes)
	{
		Logger.Debug("Plugin disposed : " + this.ToString());
	}

	public override bool InitializePlugin ()
	{
		IrcConnection.ConnectionCreated += RegisterIrcConnection;
		//Console.WriteLine("Autojoin Initialize method called"); 
		return true;
	}
	
	void RegisterIrcConnection(object sender, EventArgs e)
	{
		IrcConnection temp = (IrcConnection)sender;
		//Console.WriteLine("RegisterIrcConnection called with server value : " + temp.Info.Server);
		if (string.IsNullOrEmpty(Options.GetStringConfig(temp.Info.Server)))
			return;
		
		temp.Identified += Autojoin;
	}
	
	void Autojoin(object sender, EventArgs e)
	{
		IrcConnection temp = (IrcConnection)sender;
		string conf = Options.GetStringConfig(temp.Info.Server);
		//Console.WriteLine("Autojoin called with conf : " + conf);
		foreach (string chan in conf.Split(chanSeparator)) {
			temp.JoinChannel(chan);
			System.Threading.Thread.Sleep(20);
		}
	}
	
}
