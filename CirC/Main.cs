#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Cil;
using Circ.Backend;
using System.IO;

namespace Circ
{
	public static class CircMain
	{
		static MainControl   mainControl;
		
		public static void Main(string[] args)
		{
			Logger.Debug("Circ starting...");
			
			Logger.Debug("Initializing the Addin manager");
			Mono.Addins.AddinManager.Initialize(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), ".Circ"));
			Mono.Addins.AddinManager.Registry.Update(null);
			
			mainControl = new MainControl();
			Circ.Plugins.Shop.MainControl = mainControl;
			
			Logger.Debug("Cross your fingers, it's beginning");
			
			mainControl.WaitForInit();
			//System.Threading.Thread.Sleep(3000);
			Logger.Debug("Initialization finalized");
			
			try {
				Logger.Debug("Loading Plugins");
				PluginsContainer.LoadPlugins();
				try {
					mainControl.Idle();
				} catch (Exception ex) {
					mainControl.Frontend.ShowErrorMessage("An error has occured during the execution : " + ex.Message);
				}
			} finally {
				PluginsContainer.Unload();
				mainControl.Dispose();
			}
			
			GC.Collect();
			GC.WaitForPendingFinalizers();
		}
		
		public static void PrintUsageInformation()
		{
			Console.WriteLine("Circ, IRC client for the GNU/Linux plateform\n(c) 2006 LAVAL Jérémie\n\n"+
			"Usage information : circ [OPTIONS]\n\nOPTIONS can be"+
			":\n\t-server:hostname Connect to the specified IRC server");
			Environment.Exit(0);
		}
		
		/*private static void LoadPlugins()
		{
			plugins = new PluginLoader();
			try {
				plugins.Launch();
			} catch (PluginException e) {
				log.Error(e.Message, e);
			}
		}*/
	}
}
