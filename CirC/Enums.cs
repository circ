#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ
{
	public enum Backends {
		Cil,
		Tapioca
	}
	
	public enum Gui {
		Gtk,
		Winforms
	}
}
