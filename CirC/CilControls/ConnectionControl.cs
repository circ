#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Circ.Backend;
using Circ.Frontend;
using Circ.Controller;

namespace Circ
{

	public sealed class ConnectionControl: IConnectionControl
	{
		IrcConnection connection;
		IServerPanel frontend;
		IList<ChannelControl> chanControls = new List<ChannelControl>();
		IParser parser = DefaultParser.Instance;
		MainControl mainCtrl;
		CommandProcesser processer = CommandProcesser.Instance;
		
		public ConnectionControl(IrcConnection connection, MainControl mainCtrl)
		{
			this.connection = connection;
			this.mainCtrl   = mainCtrl;
			
			UpdatePresentation();
			System.Threading.Thread.Sleep(300);
			connection.MessageReceived += MessageReceivedHandler;
			connection.ChannelCreated += ChannelCreatedHandler;
			try {
				connection.Connect();
			} catch (ConnectionException e) {
				mainCtrl.Frontend.ShowErrorMessage(string.Format("Error while connecting to server : {0}{1}Error Message : {2}", this.connection.Info.Server,
				                                   Environment.NewLine, e.Message));
			}
			Logger.Debug("New Connection set up");
			
			InitializeProcesser();
		}
		
		internal void Dispose()
		{
			this.connection.Dispose();
		}
		
		void InitializeProcesser()
		{
			
		}
		
		public CommandProcesser Processer {
			get {
				return processer;
			}
		}
		
		public void CloseConnection()
		{
			// TODO: need to throw a event when disposing a channel so that I can update the
			// list
			// CHANGE: No need for a list in fact since the disposing of channel is handled by the Model 
			foreach (ChannelControl ctrls in chanControls)
				ctrls.CloseChannel();
		}
		
		/*public void JoinChannel (string chanName)
		{
			connection.JoinChannel(chanName);
		}*/
		
		public IrcConnection Backend {
			get {
				return connection;	
			}
		}
		
		public IServerPanel Frontend {
			get {
				return frontend;
			}
		}
		
		/*public string ServerName {
			get {
				return connection.Server;
			}
		}
		
		public string Nickname {
			get {
				return connection.Info.Pseudo;
			}
			set {
				connection.Nick = value;
			}
		}*/
		
		public IChannelControl GetChannelControl(string channelName)
		{
			IChannelControl temp = null;
			
			foreach (IChannelControl ctrl in chanControls) {
				if (channelName == ctrl.Backend.Name) {
					temp = ctrl;
					break;
				}
			}
			
			return temp;
		}
		
		internal void AddChildDiscussion(IChannelPanel panel)
		{
			// TODO: refractoring is needed
			// Basically the chain is MainCtrl -> ConnCtrl -> ChanCtrl and each element should only know it's
			// upper parent.
			// So instead of a Frontend&Backend property in MainCtrl do a static class Session and put them there
			mainCtrl.Frontend.AddDiscussion(panel);
		}
		
		void UpdatePresentation()
		{
			frontend = GuiFactory.Factory.GetServerPanel(this);
			mainCtrl.Frontend.AddServer(frontend);
			Logger.Debug("GUI updated");
		}
		
		void MessageReceivedHandler(object sender, MessageEventArgs e)
		{
			// Logger.Instance.Debug("Message received from server");
			frontend.AddNewMessage(e.Message);
		}
		
		void ChannelCreatedHandler(object sender, ChannelEventArgs e)
		{
			chanControls.Add(new ChannelControl(e.Channel, this));	
		}
	}
}
