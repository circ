#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;
using Circ.Frontend;
using Circ.Controller;

namespace Circ
{
	public sealed class ChannelControl: IChannelControl
	{
		IrcChannel chan;
		IChannelPanel frontend;
		ConnectionControl connCtrl;
		
		public ChannelControl(IrcChannel chan, ConnectionControl connCtrl)
		{
			this.chan = chan;
			this.connCtrl = connCtrl;
			
			UpdatePresentation();
			chan.NewUserList += NewUserListHandler;
			chan.MessageReceived += MessageReceivedHandler;
			chan.NewUserConnected += delegate(object sender, UserEventArgs e) {
				Logger.Debug("Adding User : " + e.User.Nick);
				frontend.AddUser(e.User.Nick);
			};
			chan.UserDisconnected += delegate (object sender, UserQuitEventArgs e) {
				Logger.Debug("Removing User : " + e.User.Nick);
				frontend.RemoveUser(e.User.Nick, e.QuitMessage);
			};
			chan.TopicUpdated += delegate (object sender, TopicEventArgs e) {
				frontend.ChangeTopic(e.Topic, "unknow", DateTime.Now);
			};
			
		}
		
		public IrcChannel Backend {
			get {
				return chan;
			}
		}
		
		public IChannelPanel Frontend {
			get {
				return frontend;
			}
		}
		
		/*public void SendMessage(string message)
		{
			chan.SendMessage(message);
		}
		
		public string ChannelName {
			get {
				return chan.Name;
			}
		}*/
		
		public void CloseChannel()
		{
		}
		
		void UpdatePresentation()
		{
			frontend = GuiFactory.Factory.GetChannelPanel(connCtrl.Frontend, this);
			connCtrl.AddChildDiscussion(frontend);
			Logger.Debug("Added new chan");
		}
		
#region Events handlers
		void NewUserListHandler(object sender, UserListEventArgs e)
		{
			Logger.Debug("New user list for : " + chan.Name);
			
			string[] usersList = new string[e.Users.Length];
			for (int i = 0; i < e.Users.Length; i++)
				usersList[i] = e.Users[i].ToString();
			
			Array.Sort<string>(usersList, StringComparer.InvariantCultureIgnoreCase);
			
			frontend.AddUser(usersList);
			
		}
		
		void MessageReceivedHandler(object sender, ChannelMessageEventArgs e)
		{
			Logger.Debug("Message received from chan : " + chan.Name);
			bool isHl = e.Message.IndexOf(chan.ParentConnection.Nick) != -1;
			frontend.AddNewMessage(DateTime.Now, e.Author, e.Message, (isHl) ? MessageFlags.Hl : MessageFlags.Default);
		}
#endregion
	}
}
