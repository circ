#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Threading;
using System.Collections.Generic;
using Circ.Backend;
using Circ.Cil;
using Circ.Frontend;
using Circ.Controller;
using Mono.Addins;

namespace Circ
{
	public sealed class MainControl: IMainControl, IDisposable
	{
		Thread presentationThread;
		bool disposed = false;
		
		IList<ConnectionControl> connControls = new List<ConnectionControl>();
		
		IFrontend frontend;
		IBackend backend;
		ConnectionFactory factory;
		CommandProcesser cmdProc = CommandProcesser.Instance;
		
		public MainControl()
		{
			IrcConnection.ConnectionCreated += ConnectionCreatedHandler;
			InitializeDefaultCommand();
			StartTheWholeThing();
		}
		
		~MainControl()
		{
			Dispose(false);
		}
		
		public void Dispose()
		{
			Dispose(true);
			System.GC.SuppressFinalize(this);
		}
		
		protected void Dispose(bool managedRes)
		{
			if (disposed)
				return;
			
			foreach (ConnectionControl ctrl in connControls)
				ctrl.Dispose();
			
			disposed = true;
		}
		
		void StartTheWholeThing()
		{
			Logger.Debug("MainControl fetching Frontend & Backend");
			frontend = AcquireFrontend(Options.GetStringConfig("frontend"));
			backend = AcquireBackend(Options.GetStringConfig("backend"));
			
			if (frontend == null || backend == null)
				Logger.Error(string.Format("Backend not loaded {0} ; Frontend not loaded : {1}", backend == null, frontend == null), null);
			
			GuiFactory.Factory = frontend.ToolkitFactory;
			ConnectionFactory.Factory = backend.GetConnectionFactory();
			
			Logger.Debug("Frontend & Backend fetched");
			InitializePresentation();
		}
		
		void InitializeDefaultCommand()
		{
			CommandProcesser.DefaultFollower = delegate(object sender, string[] args) {
				IrcConnection temp = GetConnectionFromObject(sender);
				if (temp == null) {
					Logger.Debug("Unable to get the IrcConnection");
					return;
				}
				temp.SendRawMessage(Rfc.Unknown(args));
			};
			
			cmdProc.RegisterCommand("c", delegate(object sender, string[] args) {
				if (args.Length != 2) {
					frontend.ShowErrorMessage("The connection command you typed is ill-formed. The correct form is : \"/c irc.yourserver.org yourNick\"");
					return;
				}
				ConnectionFactory.Factory.RequestConnection(Circ.Backend.ConnectionInfo.GetDefault(args[0], args[1]));
			});
			cmdProc.RegisterCommand("j", delegate(object sender, string[] args) {
				if (args.Length != 1) {
					frontend.ShowErrorMessage("Wrong parameters for the join command");
					return;
				}
				if (args[0][0] != Rfc.ChannelStartChar) {
					frontend.ShowErrorMessage("Malformed chan");
					return;
				}
				
				IrcConnection temp = GetConnectionFromObject(sender);
				if (temp == null) {
					Logger.Debug("Unable to get the IrcConnection");
					return;
				}
				temp.JoinChannel(args[0]);
			});
			cmdProc.RegisterCommand("n", delegate (object sender, string[] args) {
				if (args.Length != 1) {
					frontend.ShowErrorMessage("Wrong number of parameter for nick command");
					return;
				}
				IrcConnection temp = GetConnectionFromObject(sender);
				if (temp == null)
					return;
				temp.Nick = args[0];
			});
		}
		
		public void Idle()
		{
			presentationThread.Join();	
		}
		
		public IrcConnection ConnectNewServer(ConnectionInfo ci)
		{
			
			//1) Type typeOfGenericClass = System.Type.GetType("MyGenericClass`1")
			//2) Type typeParameters = typeof(myGenericTypeToCreate)
			//3) Type genericType = typeOfGenericClass.MakeGericType(typeparameters)
			//4) ConstructorInfo ci = genericType.GetConstrucors()[0]
			
			return factory.RequestConnection(ci);
		}
		
		public void CommandEntered(object ctrl, string cmd)
		{
			string[] args = null;
			string identifier = null;
			// If there are no space, there are no args
			if (cmd.IndexOf(' ') != -1) {
				string[] temp = cmd.Split(' ');
				identifier = temp[0];
				args = new string[temp.Length - 1];
				Array.Copy(temp, 1, args, 0, args.Length);
			} else {
				identifier = cmd;
			}
			cmdProc.ProcessCommand(ctrl, identifier, args);
		}
		
		/*public void JoinChan(string serverName, string chan)
		{
			foreach(ConnectionControl ctrl in connControls) {
				if (ctrl.Connection.Server == serverName)
					ctrl.Connection.JoinChannel(chan);
			}
		}*/
		
		public void WaitForInit()
		{
			while (!frontend.IsInitialized)
			{}
			
			System.Threading.Thread.Sleep(300);
		}
		
		public IConnectionControl GetConnectionControl(string serverName)
		{
			IConnectionControl temp = null;
			
			foreach (IConnectionControl ctrl in connControls) {
				if (ctrl.Backend.Info.Server == serverName) {
					temp = ctrl;
					break;
				}
			}
			
			return temp;
		}
		
		void InitializePresentation()
		{
			Logger.Debug("Initializing Presentation layer");
			
			presentationThread = new Thread(delegate() { 
				frontend.StartPresentation(this);
			});
			
			presentationThread.IsBackground = false;
			presentationThread.Name = "PresentationThreadChannel";
			presentationThread.Start();
		}
		
		// TODO: need refractoring : the presentation shouldn't pass directly model object
		IrcConnection GetConnectionFromObject(object input)
		{
			IrcConnection temp = null;
			if ((temp = input as IrcConnection) == null) {
				IrcChannel chanTemp;
				if ((chanTemp = input as IrcChannel) == null)
					return null;
				temp = chanTemp.ParentConnection;
			}
			return temp;
		}
		
		void ConnectionCreatedHandler(object sender, EventArgs e)
		{
			this.connControls.Add(new ConnectionControl((IrcConnection)sender, this));	
		}
		
		IFrontend AcquireFrontend(string id)
		{
			foreach (TypeExtensionNode node in AddinManager.GetExtensionNodes("/Circ/Frontends")) {
				if (node.Id == id) {
					return (IFrontend)node.CreateInstance();
				}
			}
			return null;
		}
		
		IBackend AcquireBackend(string id)
		{
			
			foreach (TypeExtensionNode node in AddinManager.GetExtensionNodes("/Circ/Backends")) {
				if (node.Id == id) {
					return (IBackend)node.CreateInstance();
				}
			}
			return null;
		}
		
		public IFrontend Frontend {
			get {
				return frontend;
			}
		}
		
		public IBackend Backend {
			get {
				return backend;
			}
		}
	}
}
