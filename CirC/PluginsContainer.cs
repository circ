#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Mono.Addins;
using Circ.Plugins;

namespace Circ
{
	public static class PluginsContainer
	{
		static IList<Plugin> pluginList = new List<Plugin>();
		
		/*~PluginsContainer()
		{
			Unload();
		}*/
			
		public static void LoadPlugins()
		{
			foreach (TypeExtensionNode node in AddinManager.GetExtensionNodes("/Circ/Plugins")) {
				try {
					Plugin temp = (Plugin)node.CreateInstance();
					if (!temp.InitializePlugin()) {
						temp.Dispose();
						return;
					}
					pluginList.Add(temp);
					Logger.Debug("New Plugin loaded : " + temp.Name);
				} catch (Exception e) {
					Logger.Error("Plugin error during loading : ", e);
				}
			}
		}
		
		public static void Unload()
		{
			foreach (Plugin plugin in pluginList) {
				try {
					plugin.Dispose();
				} catch {}
			}
		}
	}
}
