// /home/jeremie/CirCTemp/Circ.Lib/Backend/ChannelMessageBakery.cs
// Writtent by jeremie at 17:16 11/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;

namespace Circ.Backend
{
	// TODO: returning arrays isnt efficient IMO
	public delegate void ChannelBakeryDelegate<T>(IrcChannel sender, T obj);
	/*public delegate void JoinDelegate(string[] joinInfos);
	public delegate void QuitPartDelegate(string[] quitInfos);
	public delegate void TopicDelegate(string topic);
	public delegate void NewUserListDelegate(string[] users);*/
	
	public class ChannelMessageBakery
	{
		IrcChannel parent;
		string[] tokens;
		
		public event ChannelBakeryDelegate<string[]> Part;
		public event ChannelBakeryDelegate<string[]> Message;
		public event ChannelBakeryDelegate<string[]> Join;
		public event ChannelBakeryDelegate<string[]> Quit;
		public event ChannelBakeryDelegate<string>   Topic;
		public event ChannelBakeryDelegate<string[]> NewList;
		public event ChannelBakeryDelegate<string[]> NickChanged;
		
		public ChannelMessageBakery(IrcChannel parent)
		{
			this.parent = parent;
		}
		
		public void PostNewMessage(string message)
		{
			Parse(message);
			if (tokens == null)
				return;
			
			/*foreach (string s in tokens) {
				Console.Write(s);
				Console.Write("  ");
			}
			Console.WriteLine(" ");*/
			
			int code;
			if (int.TryParse(tokens[1], out code))
				HandleReply(code);
			else
				HandleAction();
		}
		
		void Parse(string message)
		{
			int separator = -1;
			for (int i = 2; i < message.Length; i++) {
				if (message[i] == ':') {
					separator = i;
					break;
				}
			}
			
			if (separator != -1) {
				string[] temp = message.Substring(0, separator - 1).Split(' ');
				tokens = new string[temp.Length + 1];
				temp.CopyTo(tokens, 0);
				tokens[tokens.Length - 1] = message.Substring(separator + 1);
			} else {
				tokens = message.Split(' ');
			}
		}
			           
		void HandleAction()
		{
			string command = (tokens[0][0] == ':') ? tokens[1] : tokens[0];
			
			if (command.Equals("PRIVMSG", StringComparison.Ordinal)) {
				if(CheckDest(2))
					return;
				
				if (Message != null)
					Message(parent, new string[] { tokens[0].Substring(1, tokens[0].IndexOf('!') - 1), tokens[3] });
			} else if (command.Equals("JOIN", StringComparison.Ordinal)) {
				// Check name
				if (CheckDest(tokens.Length - 1))
					return;
				
				int indice = tokens[0].IndexOf('!');
				string nick = tokens[0].Substring(1, indice - 1);
				if (Join != null)
					Join(parent, new string[] { nick, string.Empty });
			} else if (command.Equals("PART", StringComparison.Ordinal)) {
				// TODO: handle the multipart message (with chan separated with commas)
				if (CheckDest((tokens[0][0] == ':') ? 1 : 2))
					return;
				JoinOrQuitProcess(Part);
			} else if (command.Equals("QUIT", StringComparison.Ordinal)) {
				JoinOrQuitProcess(Quit);
			}
			
			/*Console.Write(command);
			Console.Write("         ");
			if (command.Equals("PRIVMSG", StringComparison.OrdinalIgnoreCase)) Console.WriteLine(tokens[3]);
			Console.WriteLine(" ");*/
		}
		
		void HandleReply(int code)
		{
			switch (code) {
				case Rfc.NamesReply:
					if (!tokens[4].Equals(parent.Name, StringComparison.Ordinal))
						break;
					if (NewList == null)
						break;
					string[] temp = tokens[tokens.Length - 1].Split(' ');
					temp[0] = (temp[0][0] == ':') ? temp[0].Substring(1) : temp[0];
					NewList(parent, temp);
					break;
				case Rfc.TopicReply:
					if (CheckDest(3))
						break;
					if (Topic == null)
						break;
					Topic(parent, tokens[4]);
					break;
			}
		}
		
		bool CheckDest(int index)
		{
			return !tokens[index].Equals(this.parent.Name, StringComparison.Ordinal);
		}
		
		void JoinOrQuitProcess(ChannelBakeryDelegate<string[]> evt)
		{
			if (evt == null) return;
			
			int indice = tokens[0].IndexOf('!');
			string nick = tokens[0].Substring(1, indice - 1);
			
			evt(parent, new string[] { nick, (tokens.Length >= 2) ? tokens[tokens.Length - 1] : string.Empty });
		}
		
		/*public ChannelBakeryDelegate<string[]> Message {
			set {
				if (value == null)
					return;
				mess = value;
			}
		}
		
		public ChannelBakeryDelegate<string[]> Join {
			set {
				if (value == null)
					return;
				join = value;
			}
		}
		
		public ChannelBakeryDelegate<string[]> QuitOrPart {
			set {
				if (value == null)
					return;
				quit = value;
			}
		}
		
		public ChannelBakeryDelegate<string> Topic {
			set {
				if (value == null)
					return;
				topic = value;
			}
		}
		
		public ChannelBakeryDelegate<string[]> NewUserList {
			set {
				if (value == null)
					return;
				newList = value;
			}
		}
		
		public ChannelBakeryDelegate<string[]> NickChanged {
			set {
				if (value == null)
					return;
				nickChanged = value;
			}
		}*/
	}
}
