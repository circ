#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Text;

/* Tapioca IRC reference :
account : 
server : 
fullname : telepathy-idle user http://telepathy.freedesktop.org
port : 6667
password : 
charset : UTF-8
quit-message : So long and thanks for all the IRC - telepathy-idle IRC Connection Manager for Telepathy - http://telepathy.freedesktop.org
use-ssl : False
*/

namespace Circ.Backend
{
	/// <summary>
	/// This struct store all the information needed by IrcConnection 
	/// </summary>
	/// <remarks>If the password is null or empty, it is ignored</remarks>
	public class ConnectionInfo: ICloneable
	{
		readonly string   server;
		readonly int      port;
		readonly string   password;
		readonly string   realName;
		         string   pseudo;
		         Encoding charset;
		         bool     useSsl;
				 string   quitMessage;
		         bool     passiveMode;
		
		
		
		/// <summary>
		/// Initalize a new instance of ConnectionInfo 
		/// </summary>
		/// <param name="server">The hostname of the server</param>
		/// <param name="port">The port of the server</param>
		/// <param name="pseudo">The pseudonyme of the user</param>
		/// <param name="pass">The password for the server</param>
		/// <param name="realName">The real name of the irc user</param>
		public ConnectionInfo(string server, int port, string pseudo, string pass, string realName)
		{
			this.server = server;
			this.port = port;
			this.pseudo = pseudo;
			this.password = pass;
			this.realName = realName;
			this.charset = Encoding.UTF8;
			this.useSsl = false;
			this.quitMessage = "Powered by Monologue";
			this.passiveMode = true;
		}
		
		/// <summary>
		/// The hostname of the server 
		/// </summary>
		public string Server {
			get {
				return server;
			}
		}
		
		/// <summary>
		/// The port used to connect to the server 
		/// </summary>
		public int Port {
			get {
				return port;
			}
		}
		
		/// <summary>
		/// The pseudo that will be used on the server 
		/// </summary>
		public string Pseudo {
			get {
				return pseudo;
			}
			set {
				if (value == null)
					throw new ArgumentNullException("value");
				pseudo = value;
			}
		}
		
		/// <summary>
		/// The password used to identify on Irc 
		/// </summary>
		/// <remarks>Can be null or empty</remarks>
		public string Password {
			get {
				return password;
			}
		}
		
		/// <summary>
		/// The real name of the irc user. ex: Larry The Cow 
		/// </summary>
		public string RealName {
			get {
				return realName;
			}
		}
		
		/// <summary>
		/// The charset used to display characters 
		/// </summary>
		public Encoding Charset {
			get {
				return charset;
			}
			set {
				charset = value;
			}
		}
		
		/// <summary>
		/// Do we use a secure connection ? 
		/// </summary>
		public bool UseSsl {
			get {
				return useSsl;
			}
			set {
				useSsl = value;
			}
		}
		
		public bool UsePassiveMode {
			get {
				return passiveMode;
			}
			set {
				passiveMode = value;	
			}
		}
		
		/// <summary>
		/// Message that will be displayed when exitingIRC 
		/// </summary>
		public string QuitMessage {
			get {
				return quitMessage;
			}
			set {
				quitMessage = value;
			}
		}
		
		public static ConnectionInfo GetDefault(string serv, string nick)
		{
			Circ.Backend.ConnectionInfo ci = new Circ.Backend.ConnectionInfo(serv, 6667, nick, string.Empty, nick);
			ci.QuitMessage = "Powered by Circ";
			return ci;
		}
		
		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}
