#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
//using System.Text.RegularExpressions;

namespace Circ.Backend
{
	/// <summary>
	/// A very basic parser. Not optimized at all !
	/// </summary>
	public sealed class DefaultParser: IParser
	{
		static DefaultParser instance = new DefaultParser();
		
		//static Regex privmsg = new Regex(@"^:.+!.+ PRIVMSG .*", RegexOptions.Compiled | RegexOptions.CultureInvariant);
		//static Regex reply = new Regex(@"^:.+ \d{3}.*", RegexOptions.Compiled | RegexOptions.CultureInvariant);
		
		public static IParser Instance {
			get {
				return instance;
			}
		}
		
		private DefaultParser()
		{}
		
		/// <summary>
		/// Retrieve the Author of a message 
		/// </summary>
		/// <remarks>You MUST make sure that the raw irc message is a private message before calling this</remarks>
		/// <seealso cref="RetrieveMessageType(string)">For checking if the message is a classic message</seealso>
		public string RetrieveAuthor(string rawMessage)
		{
			return rawMessage.Substring(1, rawMessage.IndexOf('!')-1);
		}
		
		public string RetrieveMessage(string rawMessage)
		{
			string param = RetrieveParameters(rawMessage);
			return param.Substring(param.IndexOf(':')+1);
		}
		
		public string RetrieveMessageTarget(string rawMessage)
		{
			string param = RetrieveParameters(rawMessage);
			return param.Substring(0, param.IndexOf(' ')); 
		}
		
		/// <summary>
		/// Retrieve the type of the raw irc message 
		/// </summary>
		/// <remarks>The raw message returned by a IrcConnection instance</remarks>
		public MessageType RetrieveMessageType(string rawMessage)
		{
			int temp;
			if (int.TryParse(GetCommand(rawMessage), out temp))
				return MessageType.Reply;
			return MessageType.Action;
		}
		
		public ActionType RetrieveAction(string rawMessage)
		{
			string action = StripFirstColon(rawMessage);
			action = action.Substring(0, action.IndexOf(' ')).ToUpperInvariant();
			ActionType temp;
			// TODO: complete the thing
			switch (action) {
				case "PING":
					temp = ActionType.Ping;
					break;
				case "JOIN":
					temp = ActionType.Join;
					break;
				case "PART":
					temp = ActionType.Part;
					break;
				case "QUIT":
					temp = ActionType.Quit;
					break;
				default:
					temp = (ActionType)0;
					break;
			}
			return temp;
		}
		
		public string RetrieveParameters(string rawMessage)
		{
			string param = StripFirstColon(rawMessage);
			param = param.Substring(param.IndexOf(' ')+1);
			return param;
		}
		
		public string RetrieveNoticeText(string rawMessage)
		{
			string noticeMessage = StripFirstColon(rawMessage);
			return noticeMessage.Substring(noticeMessage.IndexOf(':')+1);
		}
		
		public int RetrieveNumericalReply(string rawMessage)
		{
			string num = StripFirstColon(rawMessage).Substring(0, 3);
			int result;
			int.TryParse(num, out result);
			return result;
		}
		
		public string[] RetrieveUsersList(string rawMessage)
		{
			string namesList = StripFirstColon(rawMessage);
			namesList = namesList.Substring(namesList.IndexOf(':') + 1);
			return namesList.Split(' ');
		}
		
		public string RetrieveTopic(string rawMessage)
		{
			string top = rawMessage.Substring(rawMessage.IndexOf('#'));
			return top.Substring(top.IndexOf(':') + 1);
		}
		
		public string RetrieveChannelTarget(string rawMessage)
		{
			string temp = StripFirstColon(rawMessage);
			
			int dieseInd = temp.IndexOf('#');
			int spaceInd = temp.Substring(dieseInd).IndexOf(' ');
			
			// TODO : Ouille not optimized
			if (spaceInd != -1)
				temp = temp.Substring(dieseInd).Substring(0, spaceInd);
			else
				temp = temp.Substring(dieseInd);
			/*if (!(this.RetrieveMessageType(rawMessage) == MessageType.Action)) {
				temp = rawMessage.Substring(rawMessage.IndexOf('#'));
				temp = temp.Substring(0, temp.IndexOf(' '));
			}
			else {
				string without = StripFirstColon(rawMessage);
				temp = without.Substring(without.IndexOf(':') + 1);
			}*/
					
			return temp;
		}
		
		// HACK: may contain some bug as the standard describe this field as optionnal
		public string[] RetrieveJoinInformations(string rawMessage)
		{
			int indice = rawMessage.IndexOf('!');
			string nick = rawMessage.Substring(1, indice -1);
			string realName = rawMessage.Substring(indice + 1, rawMessage.IndexOf('@') - indice - 1);
			
			return new string[] { nick, realName };
		}
		
		public string[] RetrieveQuitInformations(string rawMessage)
		{
			int indice = rawMessage.IndexOf('!');
			string nick = rawMessage.Substring(1, indice -1);
			string quitMessage = StripFirstColon(rawMessage);
			quitMessage = quitMessage.Substring(quitMessage.IndexOf(':') + 1);
			
			return new string[] { nick, quitMessage };
		}

		private string StripFirstColon(string rawString)
		{
			if (rawString[0] != ':')
				return rawString;
			
			return rawString.Substring(rawString.IndexOf(' ')+1);
		}
			
		private string GetCommand(string s)
		{
			s = StripFirstColon(s);
			return s.Substring(0, s.IndexOf(' ')); 
		}
	}
}
