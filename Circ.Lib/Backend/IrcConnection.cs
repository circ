#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections;
using System.Collections.Generic;

namespace Circ.Backend
{
	public abstract class IrcConnection: IDisposable, IEnumerable<IrcChannel>
	{
		ConnectionInfo ci;
		bool disposed = false;
		
		public abstract void Connect();
		public abstract void Disconnect();
		
		/// <summary>
		/// Join the given channel
		/// </summary>
		/// <return>A struct representing the channel</return>
		public abstract IrcChannel JoinChannel(string channelName);
		
		/// <summary>
		/// Send a raw message to the server, i.e. what you type is what you get :)
		/// </summary>
		public abstract void SendRawMessage(string message);
		
		/*
		/// <summary>
		/// Send a private message to a specified channel
		/// </summary>
		public abstract void SendPrivateMessage(IrcChannel channel);
		
		/// <summary>
		/// Send a private message to the specified user
		/// </summary>
		public abstract void SendPrivateMessage(IrcUser user);
		*/
		
		public abstract IList<IrcChannel> JoinedChannels { get; }
		
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return EnumeratorHelper();
		}

		IEnumerator<IrcChannel> IEnumerable<IrcChannel>.GetEnumerator ()
		{
			return EnumeratorHelper();
		}
		
		IEnumerator<IrcChannel> EnumeratorHelper ()
		{
			foreach (IrcChannel chan in JoinedChannels) {
				yield return chan;
			}
		}
		
		protected virtual void Dispose(bool managedRes)
		{}
		
		~IrcConnection()
		{
			if (disposed)
				return;
			
			DisposeImpl(false);
			
			disposed = true;
		}
		
		public void Dispose()
		{
			if (disposed)
				return;
			
			DisposeImpl(true);
			System.GC.SuppressFinalize(this);
			
			disposed = true;
		}
		
		private void DisposeImpl(bool managedRes)
		{
			foreach (IrcChannel chan in this) {
				chan.Dispose();
			}
			Dispose(managedRes);
		}
		
		public ConnectionInfo Info {
			get {
				return (ConnectionInfo)ci.Clone();
			}
			internal set {
				ci = value;
			}
		}
		
		/*public abstract string Server { get; }*/
		public abstract string Nick { get; set; }
		public abstract string RealName { get; }
		
		public override string ToString ()
		{
			return ci.Server;
		}
		
#region events and events related stuff
		public static event EventHandler               ConnectionCreated;
		public event EventHandler                      Connecting;
		public event EventHandler                      Connected;
		public event EventHandler                      Disconnecting;
		public event EventHandler                      Disconnected;
		public event EventHandler                      Identified;
		public event EventHandler<MessageEventArgs>    MessageReceived;
		public event EventHandler<MessageEventArgs>    MessageSent;
		public event EventHandler<ChannelEventArgs>    ChannelCreated;
		public event EventHandler<NickEventArgs>       NicknameChanged;
		
		protected void OnConnecting()
		{
			if (Connecting == null)
				return;
			
				
			Connecting(this, EventArgs.Empty);
		}
		
		protected void OnConnected()
		{
			if (Connected == null)
				return;
			
				
			Connected(this, EventArgs.Empty);
		}
		
		protected void OnDisconnecting()
		{
			if (Disconnecting == null)
				return;
			
				
			Disconnecting(this, EventArgs.Empty);
		}
		
		protected void OnDisconnected()
		{
			if (Disconnected == null)
				return;
			
				
			Disconnected(this, EventArgs.Empty);
		}
		
		protected void OnIdentified()
		{
			if (Identified == null)
				return;
			
				
			Identified(this, EventArgs.Empty);
		}
		
		protected void OnMessageReceived(MessageEventArgs e)
		{
			if (MessageReceived == null)
				return;
			if (e == null)
				throw new ArgumentNullException("e");
			
			MessageReceived(this, e);
		}
		
		protected void OnMessageSent(MessageEventArgs e)
		{
			if (MessageSent == null)
				return;
			if (e == null)
				throw new ArgumentNullException("e");
			
			MessageSent(this, e);
		}
		
		protected void OnChannelCreated(ChannelEventArgs e)
		{
			if (ChannelCreated == null)
				return;
			if (e == null)
				throw new ArgumentNullException("e");
			
			ChannelCreated(this, e);
		}
		
		internal protected void NewConnectionCreated()
		{
			if (ConnectionCreated == null)
				return;
			ConnectionCreated(this, EventArgs.Empty);
		}
#endregion

	}
}