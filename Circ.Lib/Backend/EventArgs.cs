#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Net;
using System.Net.Sockets;

namespace Circ.Backend
{
	/*public class ConnectionEventArgs: EventArgs
	{
		public readonly string Server;
		public readonly int Port;
		
		public ConnectionEventArgs(string server, int port)
		{
			Server = server;
			Port = port;
		}
	}*/
	
	public class MessageEventArgs: EventArgs
	{
		public readonly string Message;
		
		public MessageEventArgs(string message)
		{
			Message = message;
		}
	}
	
	public class ChannelEventArgs: EventArgs
	{
		public readonly IrcChannel Channel;
		
		public ChannelEventArgs(IrcChannel channel)
		{
			this.Channel = channel;
		}
	}
	
	public class ChannelMessageEventArgs: EventArgs
	{
		public readonly string Author;
		public readonly string Message;
		
		public ChannelMessageEventArgs(string author, string message)
		{
			Author = author;
			Message = message;
		}
	}
	
	public class UserEventArgs: EventArgs
	{
		public readonly IrcUser User;
		public UserEventArgs(IrcUser user)
		{
			User = user;
		}
	}
	
	public class UserListEventArgs: EventArgs
	{
		public readonly IrcUser[] Users;
		public UserListEventArgs(IrcUser[] users)
		{
			Users = users;
		}
	}
	
	public class TopicEventArgs: EventArgs
	{
		public readonly string Topic;
		
		public TopicEventArgs(string topic)
		{
			this.Topic = topic;
		}
	}
	
	public class UserQuitEventArgs: UserEventArgs
	{
		public readonly string QuitMessage;
		
		public UserQuitEventArgs(string quitMessage, IrcUser user): base(user)
		{
			this.QuitMessage = quitMessage;
		}
	}
	
	public class NickEventArgs: EventArgs
	{
		public readonly string Nick;
		
		public NickEventArgs(string nick)
		{
			this.Nick = nick;
		}
	}
}
