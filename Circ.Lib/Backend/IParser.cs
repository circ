#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	public interface IParser
	{
		string RetrieveAuthor (string rawMessage);
		string RetrieveMessage (string rawMessage);
		string RetrieveMessageTarget(string rawMessages);
		ActionType RetrieveAction(string rawMessage);
		MessageType RetrieveMessageType (string rawMessage);
		int RetrieveNumericalReply(string rawMessage);
		string RetrieveNoticeText(string rawMessage);
		string RetrieveParameters(string rawMessage);
		string[] RetrieveUsersList(string rawMessage);
		string RetrieveTopic(string rawMessage);
		string RetrieveChannelTarget(string rawMessage);
		string[] RetrieveJoinInformations(string rawMessage);
		string[] RetrieveQuitInformations(string rawMessage);
	}
}
