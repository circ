#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	
	
	public class ConnectionException: ApplicationException
	{
		public ConnectionException(string message): base(message)
		{}
		
		public ConnectionException(string message, Exception inner): base(message, inner)
		{}
	}
}
