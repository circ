#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Circ.Backend
{	
	public static class Rfc
	{
		public const string Crlf = "\r\n";
		public const int    MaxSize = 512;
		public const char   HalfOpChar = '@';
		public const char   OpChar     = '&';
		public const char   NetworkOp  = '~';
		public const int    IdentDefaultPort = 113;
		public const string IdentAnswer = " : USERID : UNIX : ";
		public const char   ChannelStartChar = '#';
		
		// Consts reply code
		public const int NamesReply = 353;
		public const int TopicReply = 332;
		
		private static StringBuilder sb = new StringBuilder(30);
		
		public static bool IsMessageTooLong(string message)
		{
            if (string.IsNullOrEmpty(message))
                throw new ArgumentNullException("message");

			return message.Length > MaxSize;
		}
		
		public static string[] AdaptMessageSize(string message)
		{
			if (!IsMessageTooLong(message))
				return new string[] { message };
			
			int overflow = message.Length - MaxSize;
			IList<string> messageList = new List<string>();
			string[] temp = null;
			
			messageList.Add(message.Substring(0, MaxSize));
			
			if (overflow > MaxSize)
				foreach (string s in AdaptMessageSize(message.Substring(0, MaxSize + 1)))
					messageList.Add(s);
			else {
				messageList.Add(message.Substring(0, message.LastIndexOf(':') + 1) + message.Substring(MaxSize));
				temp = new string[messageList.Count];
				messageList.CopyTo(temp, 0);
			}
			return temp;
		}
		
		/// <summary>
		/// Concatenate a command and its parameter and add a trailing CRLF sequence.
		/// </summary>
		private static string FormatString(params object[] param)
		{
			sb.Remove(0, sb.Length);
			int i = 0;
			foreach (object s in param) {
				sb.Append(s.ToString());
				if (++i < param.Length)
					sb.Append(' ');
			}
			sb.Append(Crlf);
			return sb.ToString();
		}
		
		/*private static string FormatString(object[] param)
		{
			sb.Remove(0, sb.Length);
			int i = 0;
			foreach (object s in param) {
				sb.Append(s.ToString());
				if (++i < param.Length)
					sb.Append(' ');
			}
			sb.Append(Crlf);
			return sb.ToString();
		}*/
		
#region Standard command
		public static string Pass(string password)
		{
			return FormatString("PASS", password);
		}
		
		public static string Nick(string nickname)
		{
			return FormatString("NICK", nickname);
		}
		
		public static string User(string name, string realName)
		{
			return FormatString("USER", name, name, "irc.epiknet.org", ':'+realName); 
		}
		
		public static string Quit(string quitMessage)
		{
			if (string.IsNullOrEmpty(quitMessage))
				return FormatString("QUIT");
			return FormatString("QUIT", quitMessage);
		}
		
		public static string Pong(string args)
		{
			return FormatString("PONG", args);
		}
		
		public static string Join(string chan)
		{
			return FormatString("JOIN", chan);
		}
		
		public static string Part(string channel, string quitMessage)
		{
			if (string.IsNullOrEmpty(quitMessage))
				return FormatString("PART", channel);
			return FormatString("PART", channel, quitMessage);
		}
		
		public static string PrivMsg(string target, string mess)
		{
			return FormatString("PRIVMSG", target, ':'+mess);
		}
		
		// This is used to construct a raw command provided by the user
		public static string Unknown(string[] args)
		{
			args[0] = args[0].ToUpperInvariant();
			args[args.Length - 1] = ':' + args[args.Length - 1];
			return FormatString(args);
		}
#endregion
	}
}
