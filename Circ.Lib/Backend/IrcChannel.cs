#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	public abstract class IrcChannel: IDisposable
	{
		string channelName;
		IrcConnection connection;
		
		public IrcChannel(string channelName, IrcConnection ic)
		{
			this.channelName = channelName;
			this.connection = ic;
		}
		
		~IrcChannel()
		{
			DisposeImpl(false);
		}
		
		public void Dispose()
		{
			DisposeImpl(true);
			System.GC.SuppressFinalize(this);
		}
		
		void DisposeImpl(bool managedRes)
		{
			this.SendQuitMessage(connection.Info.QuitMessage);
			Dispose(managedRes);
		}
		
		protected virtual void Dispose(bool managedRes)
		{
		}
		
		public string Name {
			get {
				return channelName;
			}
		}
		
		public IrcConnection ParentConnection {
			get {
				return connection;
			}
		}
		
		// Method to implement for backend :
		//public abstract IrcUser[] AllUsers { get; }
		//public abstract IrcUser[] UsersPresent { get; }
		//public abstract IrcUser[] UsersAway { get; }
		
		protected abstract void SendQuitMessage(string byeMessage);
		public abstract void SendMessage(string message);
		
		public override string ToString ()
		{
			return channelName;
		}

		
#region events and events related stuff
		public static event EventHandler<EventArgs>        NewChannelJoined;
		public event EventHandler<ChannelMessageEventArgs> MessageReceived;
		public event EventHandler<UserEventArgs>           NewUserConnected;
		public event EventHandler<UserQuitEventArgs>       UserDisconnected;
		public event EventHandler<UserListEventArgs>       NewUserList;
		public event EventHandler<TopicEventArgs>          TopicUpdated;
		
		
		protected void OnNewChannelJoined()
		{
			if (NewChannelJoined == null)
				return;
			NewChannelJoined(this, EventArgs.Empty);
		}
		
		protected void OnMessageReceived(ChannelMessageEventArgs e)
		{
			if (MessageReceived == null)
				return;
			MessageReceived(this, e);
		}
		
		protected void OnNewUserConnected(UserEventArgs e)
		{
			if (NewUserConnected == null)
				return;
			NewUserConnected(this, e);
		}
		
		protected void OnUserDisconnected(UserQuitEventArgs e)
		{
			if (UserDisconnected == null)
				return;
			UserDisconnected(this, e);
		}
		
		protected void OnNewUserList(UserListEventArgs e)
		{
			if (NewUserList == null)
				return;
			NewUserList(this, e);
		}
		
		protected void OnTopicUpdated(TopicEventArgs e)
		{
			if (TopicUpdated == null)
				return;
			TopicUpdated(this, e);
		}
#endregion
	}
}
