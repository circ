#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	// <T> where T : IrcConnection, new()
	public abstract class ConnectionFactory
	{
		static ConnectionFactory factory;
		
		public static ConnectionFactory Factory {
			get {
				return factory;
			}
			set {
				if (value == null)
					return;
			
				factory = value;
			}
		}
		
		/*public ConnectionFactory(Type t)
		{
			if (!t.IsSubclassOf(typeof(IrcConnection)))
				throw new ArgumentException("t need to be a subclass of IrcConnection", "t");
			
			connection = t;
		}*/
		
		public abstract IrcConnection RequestConnection (ConnectionInfo ci);
		/*{
			IrcConnection temp = (IrcConnection)Activator.CreateInstance(connection);

			temp.UseConnectionInfo(ci);
			temp.NewConnectionCreated();
			return temp;	
		}*/
	}
	
	// Generic version
	public sealed class ConnectionFactory<T>: ConnectionFactory where T : IrcConnection, new()
	{
		public override IrcConnection RequestConnection (ConnectionInfo ci)
		{
			IrcConnection temp = new T();
			
			temp.Info = ci;
			temp.NewConnectionCreated();
			return temp;
		}
	}
}
