#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	public abstract class IrcUser
	{
		readonly string     pseudo;
		readonly string     realName;
		readonly IrcChannel parentChannel;
		
		public IrcUser(string pseudo, string realName, IrcChannel parentChannel)
		{
			this.pseudo   = pseudo;
			this.realName = realName;
			this.parentChannel = parentChannel;
		}
		
		public string Nick {
			get {
				return pseudo;
			}
		}
		
		public string RealName {
			get {
				return realName;
			}
		}
		
		public IrcChannel ParentChannel {
			get {
				return parentChannel;
			}
		}
		
		public override string ToString ()
		{
			return pseudo;
		}

		
		//public abstract void Ban();
		//public abstract void Kick();
		//public abstract void Voice();
		
		/// <summary>
		/// Send a private message to this user
		/// </summary>
		public abstract void PrivMessage(string message);
		
		public abstract bool IsOp { get; set; }
		public abstract bool IsHalfOp { get; set; }
	}
}
