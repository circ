#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	public enum MessageType {
		Action = 1,
		Reply
	}
	
	public enum ActionType {
		PrivateMessage = 1,
		Join,
		Part,
		Mode,
		Topic,
		Names,
		List,
		Invite,
		Kick,
		Whois,
		Ping,
		Pong,
		Quit,
		Error,
		Notice,
	}
}
