#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Backend
{
	
	[Mono.Addins.TypeExtensionPoint("/Circ/Backends", Name="IRC Backends",
	                                Description="This extension point is used to load IRC backend engine to be used by Circ")]
	public interface IBackend
	{
		ConnectionFactory GetConnectionFactory();
		
		string Name { get; }
	}
}
