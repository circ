
using System;
using System.Collections.Generic;
using System.Text;

namespace Circ.Lib
{
	class StringsTree
	{
		Dictionary<char, TreeNode> root = new Dictionary<char, TreeNode>();
		StringBuilder sb = new StringBuilder(25);
		
		public StringsTree(string[] nicks)
		{
			BuildTree(nicks);
		}
		
		public string FindApproximately(string partialNick)
		{
			TreeNode temp;
			if (!root.TryGetValue(partialNick[0], out temp))
				return null;
			
			// Clean the buffer
			sb.Remove(0, sb.Length);
			// Add first character
			sb.Append(partialNick[0]);
			
			for (int i = 1; i < partialNick.Length; i++) {
				
			}
		}
		
		void 
		
		void BuildTree(string[] nicks)
		{
			
		}
		
		private class TreeNode
		{
			TreeNode                   father;
			Dictionary<char, List<TreeNode>> childNodes = new Dictionary<char, List<TreeNode>>();
			char                       value;
			
			public TreeNode(TreeNode father, char value)
			{
				this.father = father;
				this.value  = value;
			}
			
			public void AddChild(TreeNode child)
			{
				if (!childNodes.ContainsKey(child.Value)) {
					List<TreeNode> temp = new List<TreeNode>();
					temp.Add(child);
					this.childNodes.Add(child.Value, temp);
				} else {
					this.childNodes[child.Value].Add(child);
				}
			}
			
			public char Value {
				get {
					return value;
				}
			}
			
			public TreeNode Father {
				get {
					return father;
				}
			}
		}
	}
}
