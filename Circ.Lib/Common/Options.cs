#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using Nini.Config;
using Nini;

namespace Circ
{
	public static class Options
	{
		static CommandLine cmd;
		static Configuration config;
		
		static Options()
		{
			cmd = new CommandLine(Environment.GetCommandLineArgs());
			if (cmd.IsDefined("C"))
				config = new Configuration(cmd["C"]);
			else
				config = new Configuration();
		}
		
		public static bool GetBooleanConfig(string option)
		{
			return cmd.IsDefined(option) ? true : config.InternalGetBooleanConfig(option, GetSection());
		}
		
		public static string GetStringConfig(string option)
		{
			return cmd.IsDefined(option) ? cmd[option] : config.InternalGetStringConfig(option, GetSection());
		}
		
		public static void SetConfig(string option, string value)
		{
			config.InternalSetStringConfig(option, value, GetSection());
		}
		
		public static void SetConfig(string option, bool value)
		{
			config.InternalSetBooleanConfig(option, value, GetSection());
		}
		
		static string GetSection()
		{
			StackTrace st = new StackTrace();
			StackFrame sf = st.GetFrame(2);
			
			return (sf == null) ? "General" : sf.GetMethod().ReflectedType.Assembly.GetName().Name;
		}
		
		/*public static bool Plugins {
			get {
				return (cmd.IsDefined("Uplugins")) ? true : config.Plugins;
			}
		}
		
		public static string ServersListPath {, Section sect
			get {
				return (cmd.IsDefined("slist")) ? cmd["slist"] : config.ServersListPath;
			}
		}
		
		public static string Backend {
			get {
				return (cmd.IsDefined("backend")) ? cmd["backend"] : config.Backend;
			}
		}
		
		public static string Frontend {
			get {
				return (cmd.IsDefined("frontend")) ? cmd["frontend"] : config.Frontend;
			}
		}
		
		private Backends BackendFromString(string backend)
		{
			Backends temp = Backends.Cil;
			switch (backend) {
				case "Cil":
					break;
				case "Tapioca":
					temp = Backends.Tapioca;
					break;
				default:
					break;
			}
			return temp;
		}*/
		
		private class Configuration
		{
			FileInfo fileInfo;
			IConfigSource source = null;
			/*IConfig controller;
			IConfig backend;
			IConfig frontend;*/
			//Dictionary<string, IConfig> dict = new Dictionary<string,Nini.Config.IConfig>();
			
			internal Configuration(string filePath)
			{
				fileInfo = new FileInfo(filePath);
				
				if (!fileInfo.Exists) {
					if (!fileInfo.Directory.Exists)
						fileInfo.Directory.Create();
					StreamWriter sw = new StreamWriter(fileInfo.OpenWrite());
					Type t = typeof(Configuration);
					StreamReader sr = new StreamReader(t.Assembly.GetManifestResourceStream("Circ.conf"));
					sw.Write(sr.ReadToEnd());
					sw.Dispose();
					sr.Dispose();
				}
				
				source = new IniConfigSource(fileInfo.OpenRead());
				
				/*controller = source.Configs["Controller"];
				backend = source.Configs["Backend"];
				frontend = source.Configs["Frontend"];*/
				
				/* AliasText alias = new AliasText();
				alias.AddAlias("true", true);
				alias.AddAlias("false", false);
				connection.Alias = alias; */
			}
			
			public Configuration():
				this(Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), ".Circ"), "Circ.conf"))
			{
			}
			
			public bool InternalGetBooleanConfig(string option, string sectionName)
			{
				return source.Configs[sectionName].GetBoolean(option, false);
			}
			
			public string InternalGetStringConfig(string option, string sectionName)
			{
				return source.Configs[sectionName].GetString(option, string.Empty);
			}
			
			public void InternalSetStringConfig(string option, string value, string sectionName)
			{
				source.Configs[sectionName].Set(option, value);
			}
		
			public void InternalSetBooleanConfig(string option, bool value, string sectionName)
			{
				source.Configs[sectionName].Set(option, value);
			}			
			
			//public void InternalSetBoolean
			
			/*public string Frontend {
				get {
					// FIXME: WTF ? Nini seems poised
					//return (general.Contains("Frontend")) ? general.GetString("Frontend") : "Gtk";
					return "Gtk";
				}
			}
			
			public string Backend {
				get {
					//return general.GetString("Backend", "Cil");
					return "Cil";
				}
			}
			
			public bool Plugins {
				get {
					return general.GetBoolean("EnablePlugins", true);
				}
			}
			
			public bool AutoReconnect {
				get {
					return connection.GetBoolean("AutoReconnect", true);
				}
			}
			
			public string ServersListPath {
				get {
					// Mouhaha, Path.Combine things tend to get larger than 80 columns :p
					return connection.GetString("ServersListPath",
					                                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal),
					                                             	".circ"));
				}
			}*/
		}
		
		/// <summary>
		/// Parse the parameters given on the command line
		/// </summary>
		private class CommandLine
		{
			string[]    args;
			IDictionary<string, string> options;
			//protected Option[]  options;
			string[]  names;
			char      argSeparator = ':';
			char      argId        = '-';
			int       nameIndex    = -1;
			
			public CommandLine(string[] argLine)
			{
	            if (argLine == null)
	                throw new ArgumentNullException("argLine");

				args = new string[argLine.Length];
				argLine.CopyTo(args, 0);
			}
			
			public CommandLine(string[] argLine, char argument, char separator) : 
				this(argLine)
			{
				argSeparator = separator;
				argId        = argument;
			}
			
			/*public Option[] Options {
				get {
					if (options == null)
						ParseOptions();
					return options;
				}
			}
			*/
			
			public string[] Names {
				get {
					if (names == null)
						ParseNames();
	                string[] namesTemp = new string[names.Length];
	                Array.Copy(names, namesTemp, names.Length);
					return namesTemp;
				}
			}
			
			public bool IsDefined(string option)
			{
				if (options == null)
					ParseOptions();
				return options.ContainsKey(option);
			}
			
			public string this[string key] {
				get {
					return options[key];
				}
			}
			
			protected void ParseOptions()
			{
				if (options != null)
					return;
					
				options = new Dictionary<string, string>();
				
				int index = -1;
				
				foreach (string s in args) {
					if (s[0] == argId) {
						index++;
						int indexOfSeparator = s.IndexOf(argSeparator);
						
						// Determine the option name be it with a value or a switch
						string optionName = (indexOfSeparator != -1) ?
							s.Substring(1, (indexOfSeparator - 1)) : s.Substring(1);
						
						// Retrieve the associated parameter value if any
						string optionValue = (indexOfSeparator != -1) ?
							s.Substring(indexOfSeparator + 1) : null;
						
						options.Add(optionName, optionValue);
					}
					else {
						break;
					}
				}
				
				nameIndex = index + 1;
			}
			
			protected void ParseNames()
			{
				ICollection<string> temp = new List<string>();
				
				if (nameIndex != -1)
					for (int i = nameIndex; i < args.Length; i++)
						temp.Add(args[i]);
				else
					foreach (string s in args)
						if (s[0] != argId)
							temp.Add(s);
				
				if (temp.Count != 0) {
					names = new string[temp.Count];
					temp.CopyTo(names, 0);
				}
			}
		}
	}
}
