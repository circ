#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace Circ
{
	public enum LogType {
		Debug,
		Error
	}
	
	public static class Logger
	{
		static TextWriter sw;
		
		static Logger()
		{
			sw = Console.Out;
		}
		
		public static TextWriter Output {
            get  {
                return sw;
            }
			set {
				if (value == null)
					throw new ArgumentNullException("value");
                sw.Dispose();
				sw = value;
			}
		}
		
		private static void Log(LogType type, string mess)
		{
			StackTrace st = new StackTrace();
			StackFrame sf = st.GetFrame(2);
			
			if (mess == null || sf == null)
				return;
			
			sw.WriteLine(type.ToString()+" -> "+ sf.GetMethod().Name+" :: "+mess);
		}
		
		[System.Diagnostics.ConditionalAttribute("__DEBUG__")]
		public static void Debug(string mess)
		{
			Log(LogType.Debug, mess);
		}
		
		public static void Error(string mess, Exception ex)
		{
			string message = (ex == null) ? mess : (mess + ", Exception type : " + ex.GetType().ToString()
				+ ", Exception message : " + ex.Message);
			Console.ForegroundColor = ConsoleColor.DarkRed;
			Log(LogType.Error, message);
			Console.ResetColor();
		}
	}
}
