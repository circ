
using System;
using System.Collections.Generic;

namespace Circ.Lib
{
	public delegate T Transformation<T>(T input);
	public delegate void TransformationHandler<T>(TransformationPool<T> sender, T data);
	
	public class TransformationPool<T>
	{
		Transformation<T> prepend;
		Queue<Transformation<T>> transList;
		Transformation<T> last;
		
		object sync = new object();
		
		public event TransformationHandler<T> Output;
		
		public void Input(T item)
		{
			if (Output == null)
				return;
			
			// The adding must remain in order hence the lock
			lock (sync) {
				T temp = (prepend == null) ? item : prepend(item);
				foreach(Transformation<T> trans in transList)
					temp = trans(temp);
				Output(this, (last == null) ? temp : last(temp));
			}
		}
		
		public void BeginInput(T item)
		{
			if (Output == null)
					return;
			
			// The adding must remain in order hence the lock
			lock(sync) {
				System.Threading.ThreadPool.QueueUserWorkItem(delegate {
					T temp = (prepend == null) ? item : prepend(item);
					foreach(Transformation<T> trans in transList)
						temp = trans(temp);
					Output(this, (last == null) ? temp : last(temp));
				});
			}
		}
		
		public void AddTransformation(Transformation<T> trans)
		{
			transList.Enqueue(trans);
		}
		
		public Transformation<T> Preprend {
			get {
				return prepend;
			}
			set {
				prepend = value;
			}
		}
		
		public Transformation<T> Last {
			get {
				return last;
			}
			set {
				last = value;
			}
		}
	}
}
