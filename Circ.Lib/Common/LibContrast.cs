// Author of the original C version : David Trowbridge
// Ported to C# by LAVAL Jérémie
// This file is licenced under the MIT licence
using System;

namespace Circ.Lib
{
	public static class LibContrast
	{
		static float[][] color_regions =  new float[23][];
			
		static LibContrast()
		{
			color_regions[0] = new float[] {40.0f,  60.0f, -100.0f, -80.0f,  -10.0f,  20.0f}; /* CONTRAST_COLOR_AQUA */
			color_regions[1] = new float[] { 0.0f,  30.0f,    0.0f,   0.0f,    0.0f,   0.0f}; /* CONTRAST_COLOR_BLACK */
			color_regions[2] = new float[] {25.0f,  35.0f, -100.0f,   0.0f, -100.0f, -50.0f}; /* CONTRAST_COLOR_BLUE */
			color_regions[3] = new float[] {30.0f,  60.0f,   30.0f,  50.0f,   70.0f, 100.0f}; /* CONTRAST_COLOR_BROWN */
			color_regions[4] = new float[] {50.0f,  65.0f, -100.0f, -30.0f, -100.0f, -50.0f}; /* CONTRAST_COLOR_CYAN */
			color_regions[5] = new float[] { 0.0f,  20.0f,  -40.0f,  50.0f, -100.0f, -60.0f}; /* CONTRAST_COLOR_DARK_BLUE */
			color_regions[6] = new float[] {20.0f,  35.0f, -100.0f, -70.0f,   60.0f, 100.0f}; /* CONTRAST_COLOR_DARK_GREEN */
			color_regions[7] = new float[] {20.0f,  40.0f,    0.0f,   0.0f,    0.0f,   0.0f}; /* CONTRAST_COLOR_DARK_GREY */
			color_regions[8] = new float[] {10.0f,  40.0f,   90.0f, 100.0f,   70.0f, 100.0f}; /* CONTRAST_COLOR_DARK_RED */
			color_regions[9] = new float[] {15.0f,  40.0f, -100.0f, -80.0f,   80.0f, 100.0f}; /* CONTRAST_COLOR_GREEN */
			color_regions[10] = new float[] {35.0f,  60.0f,    0.0f,   0.0f,    0.0f,   0.0f}; /* CONTRAST_COLOR_GREY */
			color_regions[11] = new float[] {40.0f,  50.0f, -100.0f,   0.0f, -100.0f, -60.0f}; /* CONTRAST_COLOR_LIGHT_BLUE */
			color_regions[12] = new float[] {60.0f,  75.0f,   30.0f,  50.0f,   80.0f, 100.0f}; /* CONTRAST_COLOR_LIGHT_BROWN */
			color_regions[13] = new float[] {80.0f,  90.0f, -100.0f, -70.0f,   70.0f, 100.0f}; /* CONTRAST_COLOR_LIGHT_GREEN */
			color_regions[14] = new float[] {50.0f,  80.0f,    0.0f,   0.0f,    0.0f,   0.0f}; /* CONTRAST_COLOR_LIGHT_GREY */
			color_regions[15] = new float[] {55.0f,  65.0f,   80.0f,  90.0f,   75.0f, 100.0f}; /* CONTRAST_COLOR_LIGHT_RED */
			color_regions[16] = new float[] {40.0f,  55.0f,   90.0f, 100.0f,  -50.0f,   0.0f}; /* CONTRAST_COLOR_MAGENTA */
			color_regions[17] = new float[] {65.0f,  80.0f,   20.0f,  65.0f,   90.0f, 100.0f}; /* CONTRAST_COLOR_ORANGE */
			color_regions[18] = new float[] {35.0f,  45.0f,   85.0f, 100.0f,  -90.0f, -80.0f}; /* CONTRAST_COLOR_PURPLE */
			color_regions[19] = new float[] {40.0f,  50.0f,   80.0f, 100.0f,   75.0f, 100.0f}; /* CONTRAST_COLOR_RED */
			color_regions[20] = new float[] {70.0f,  95.0f,   90.0f, 100.0f, -100.0f,   0.0f}; /* CONTRAST_COLOR_VIOLET */
			color_regions[21] = new float[] {75.0f, 100.0f,    0.0f,   0.0f,    0.0f,   0.0f}; /* CONTRAST_COLOR_WHITE */
			color_regions[22] = new float[] {90.0f, 100.0f,    5.0f,  15.0f,   92.5f, 105.0f}; /* CONTRAST_COLOR_YELLOW */
		}
		
		static int Clamp(int value, int min, int max)
        {
            // First we check to see if we're greater than the max
            value = (value > max) ? max : value;

            // Then we check to see if we're less than the min.
            value = (value < min) ? min : value;

            // There's no check to see if min > max.
            return value;
        }
		
		/*
		 * This performs the non-linear transformation that accounts for the gamma curve
		 * in sRGB, but avoids numerical problems.
		 */
		static float
		srgb_to_xyz_g(float K)
		{
			float a     = 0.055f;
			float gamma = 2.4f;

			if (K > 0.04045)
				return (float)Math.Pow((K + a) / (1 + a), gamma);
			else
				return K / 12.92f;
		}

		static float
		xyz_to_lab_f(float t)
		{
			if (t > 0.008856f)
				return (float)Math.Pow(t, 1/3.0f);
			else
				return 7.787f*t + 16f/116.0f;
		}

		static void
		rgb_to_lab(ushort  R,
		           ushort  G,
		           ushort  B,
		           out float L,
		           out float a,
		           out float b)
		{
			float x, y, z, gr, gg, gb, fy;

			/* This is the reference white point.  Since we're treating "RGB" as
			 * sRGB, this is the D65 point.
			 */
			float Xn = 0.93819f;
			float Yn = 0.98705f;
			float Zn = 1.07475f;

			gr = srgb_to_xyz_g(R / 65535.0f);
			gg = srgb_to_xyz_g(G / 65535.0f);
			gb = srgb_to_xyz_g(B / 65535.0f);

			x = 0.412424f * gr + 0.357579f * gg + 0.180464f * gb;
			y = 0.212656f * gr + 0.715158f * gg + 0.072186f * gb;
			z = 0.019332f * gr + 0.119193f * gg + 0.950444f * gb;

			fy = xyz_to_lab_f(y / Yn);
			L = 116 * fy - 16;
			a = 500 * (xyz_to_lab_f(x / Xn) - fy);
			b = 200 * (fy - xyz_to_lab_f(z / Zn));
		}

		static float
		xyz_to_srgb_C(float K)
		{
			float a     = 0.055f;
			float gamma = 2.4f;

			if (K > 0.00304)
				return (1 + a) * (float)Math.Pow(K, (1.0 / gamma)) - a;
			else
				return K * 12.92f;
		}

		static void
		lab_to_rgb(float L,
		           float a,
		           float b,
		           out ushort R,
		           out ushort G,
		           out ushort B)
		{
			float x, y, z, fy, fx, fz, delta, delta2, rs, gs, bs;
			float Xn = 0.93819f;
			float Yn = 0.98705f;
			float Zn = 1.07475f;

			fy = (L + 16) / 116;
			fx = fy + a / 500;
			fz = fy - b / 200;
			delta = 6.0f / 29;
			delta2 = (float)Math.Pow(delta, 2);

			if (fx > delta) x = Xn * (float)Math.Pow(fx, 3);
			else            x = (fx - 16.0f/116) * 3 * delta2 * Xn;

			if (fy > delta) y = Yn * (float)Math.Pow(fy, 3);
			else            y = (fy - 16.0f/116) * 3 * delta2 * Yn;

			if (fz > delta) z = Zn * (float)Math.Pow(fz, 3);
			else            z = (fz - 16.0f/116) * 3 * delta2 * Zn;

			rs =  3.2410f * x - 1.5374f * y - 0.4986f * z;
			gs = -0.9692f * x + 1.8760f * y + 0.0416f * z;
			bs =  0.0556f * x - 0.2040f * y + 1.0570f * z;

			R = (ushort)Clamp((int) Math.Round(xyz_to_srgb_C(rs) * 65535), 0, 65535);
			G = (ushort)Clamp((int) Math.Round(xyz_to_srgb_C(gs) * 65535), 0, 65535);
			B = (ushort)Clamp((int) Math.Round(xyz_to_srgb_C(bs) * 65535), 0, 65535);
		}

		static float
		lab_distance(float La,
		             float aa,
		             float ba,
		             float Lb,
		             float ab,
		             float bb)
		{
		  float dL, da, db;
		  dL = Math.Abs(Lb - La);
		  da = Math.Abs(ab - aa);
		  db = Math.Abs(bb - ba);
		  return (float)Math.Sqrt(dL*dL + da*da + db*db);
		}
		
		public static RgbColor GetBestColorFromBackground(RgbColor background, ColorPalette color)
		{
			float L, a, b;
			int max_color;
			ushort R, G, B;
			float max_dist;
			float[][] points = new float[8][];
			float ld, cd;
			int i = 0;
			RgbColor rcolor;
			
			for (i = 0; i < 8; i++)
				points[i] = new float[3];

			rgb_to_lab(background.Red, background.Green, background.Blue, out L, out a, out b);

			points[0][0] = color_regions[(int)color][0]; points[0][1] = color_regions[(int)color][2]; points[0][2] = color_regions[(int)color][4];
			points[1][0] = color_regions[(int)color][0]; points[1][1] = color_regions[(int)color][2]; points[1][2] = color_regions[(int)color][5];
			points[2][0] = color_regions[(int)color][0]; points[2][1] = color_regions[(int)color][3]; points[2][2] = color_regions[(int)color][4];
			points[3][0] = color_regions[(int)color][0]; points[3][1] = color_regions[(int)color][3]; points[3][2] = color_regions[(int)color][5];
			points[4][0] = color_regions[(int)color][1]; points[4][1] = color_regions[(int)color][2]; points[4][2] = color_regions[(int)color][4];
			points[5][0] = color_regions[(int)color][1]; points[5][1] = color_regions[(int)color][2]; points[5][2] = color_regions[(int)color][5];
			points[6][0] = color_regions[(int)color][1]; points[6][1] = color_regions[(int)color][3]; points[6][2] = color_regions[(int)color][4];
			points[7][0] = color_regions[(int)color][1]; points[7][1] = color_regions[(int)color][3]; points[7][2] = color_regions[(int)color][5];

			max_dist = 0;
			max_color = 0;
			for (i = 0; i < 8; i++) {
				float dist = lab_distance(L, a, b, points[i][0], points[i][1], points[i][2]);
				if (dist > max_dist) {
					max_dist = dist;
					max_color = i;
				}
			}

			/*
			 * If the luminosity distance is really short, extend the vector further
			 * out.  This may push it outside the bounds of the region that a color
			 * is specified in, but it keeps things readable when the background and
			 * foreground are really close.
			 */
			ld = Math.Abs(L - points[max_color][0]);
			cd = (float)Math.Sqrt(Math.Pow(Math.Abs(a - points[max_color][1]), 2) + Math.Pow(Math.Abs(b - points[max_color][2]), 2));
			if ((ld < 10.0f) && (cd < 60.0f)) {
				float dL, da, db;
				dL = points[max_color][0] - L;
				da = points[max_color][1] - a;
				db = points[max_color][2] - b;
				points[max_color][0] = L + (dL * 4.0f);
				points[max_color][1] = a + (da * 1.5f);
				points[max_color][2] = b + (db * 1.5f);
			}

			lab_to_rgb(points[max_color][0],
			           points[max_color][1],
			           points[max_color][2],
			           out R,
			           out G,
			           out B);
			
			rcolor.Red = (byte)(R / 257);
			rcolor.Green = (byte)(G / 257);
			rcolor.Blue = (byte)(B / 257);
			
			return rcolor;	
		}
	}
	
	public struct RgbColor {
		public RgbColor(byte red, byte green, byte blue)
		{
			this.Red = red;
			this.Blue = blue;
			this.Green = green;
		}
		
		public byte Red;
		public byte Blue;
		public byte Green;
	}
	
	public enum ColorPalette {
		Aqua        =  0,
		Black       =  1,
		Blue        =  2,
		Brown       =  3,
		Cyan        =  4,
		DarkBlue    =  5,
		DarkGreen   =  6,
		DarkGrey    =  7,
		DarkRed     =  8,
		Green       =  9,
		Grey        = 10,
		LightBlue   = 11,
		LightBrown  = 12,
		LightGreen  = 13,
		LightGrey   = 14,
		LightRed    = 15,
		Magenta     = 16,
		Orange      = 17,
		Purple      = 18,
		Red         = 19,
		Violet      = 20,
		White       = 21,
		Yellow      = 22,
		Last        = 23,
	}
}
