#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Frontend
{
	[FlagsAttribute]
	public enum MessageFlags {
		Default = 0x0,
		Notice = 0x01,
		Hl = 0x2,
		Self = 0x4
	}
}
