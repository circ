#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Frontend
{
	public interface IChannelPanel
	{	
		void AddNewMessage(DateTime timestamp, string author, string message, MessageFlags flags);
		//void AddNewNotice(string notice);
		void ChangeTopic(string topic, string userWhoModified, DateTime timestamp);
		
		void AddUser(string[] users);
		void AddUser(string user);
		void RemoveUser(string user, string quitMessage);
		
		bool UseEmoticons { get; set; }
		bool UseUrlHighlight { get; set; }
	}
}
