#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Controller;

namespace Circ.Frontend
{
	/// <summary>
	/// Represent the Presentation associated to the main Window
	/// </summary>
	[Mono.Addins.TypeExtensionPoint("/Circ/Frontends", Name="GUI Frontends", Description="This extension point is used to load the prefered host user interface for Circ")]
	public interface IFrontend
	{
		void StartPresentation(IMainControl ctrl);
		void StopPresentation();
		
		void AddServer(IServerPanel serverPanel);
		void AddDiscussion(IChannelPanel discussionsPanel);
		
		void ShowErrorMessage(string message);
		
		IFactory ToolkitFactory { get; }
		string Name { get; }
		
		bool IsInitialized { get; }
	}
}
