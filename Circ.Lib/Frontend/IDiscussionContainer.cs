#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Frontend
{
	
	
	public interface IDiscussionsContainer
	{
		//void AddDiscussion(string channelName, IMessagesPanel messages, IUsersPanel users);
		//void AddServer(string serverName, IServerPanel server);
		int NumberOfDiscussions { get; }
		int NumberOfServers { get; }
	}
}
