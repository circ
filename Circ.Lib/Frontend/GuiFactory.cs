#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Frontend
{
	public static class GuiFactory
	{
		static IFactory facto;
		
		public static IFactory Factory {
			get {
				return facto;	
			}
			set {
				if (value == null)
					throw new ArgumentNullException("value");
				facto = value;
			}
		}
	}
}
