#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;
using Circ.Controller;
using Circ.Lib;

namespace Circ.Plugins
{
	[Mono.Addins.TypeExtensionPoint("/Circ/Plugins", Name="Plugins", Description="Standard extension point for every Plugins")]
	public abstract class Plugin
	{
#region Things that people should implement
		/// <summary>
		/// Ask the names of the authors of the plugin
		/// </summary>
		/// <return>The names of the authors</return>
		public abstract string[] Authors { get; }
		
		/// <summary>
		/// Ask the name of the plugin
		/// </summary>
		/// <return>The name of the plugin</return>
		public abstract string   Name { get; }
		
		/// <summary>
		/// Ask the description of the plugin
		/// </summary>
		/// <return>The description of the plugin</return>
		public abstract string   Description { get; }
		
		/// <summary>
		/// Ask the version in textual form of the plugin 
		/// </summary>
		/// <return>the version of the plugin</return>
		public abstract string   Version { get; }
		
		/// <summary>
		/// This method is used by the plugin to allocate the ressource it might need
		/// </summary>
		/// <return>Return true if the operation was successful, false if a problem happened</return>
		/// <remarks>If the method return false, the Dispose method is called and the plugin isn't added to the factory</remarks>
		public abstract bool InitializePlugin();
		
		~Plugin()
		{
			Dispose(false);	
		}
		
		public void Dispose()
		{
			Dispose(true);
			System.GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// This method is used by the plugin to dispose its ressources (protected version)
		/// </summary>
		protected virtual void Dispose(bool managedRes)
		{
		}
		
		/*
		/// <summary>
		/// If your plugin provides services via commands (like the classical ones i.e. /nick, /away etc...)
		/// return them here
		/// </summary>
		/// <remarks>Return just the name of the command (ex : foo and not /foo)</remarks>
		public abstract string[] CommandsProvided { get; }
		
		/// <summary>
		/// Give the command and its parameters to the plugin to allo it to do its job
		/// </summary>
		/// <param name="cmd">the command and its parameter</param>
		/// <remarks>The trailing slash (/) is stripped from the command (ex: "foo bar" and not "/foo bar"</remarks>
		public abstract void ParseCommand(string cmd);
#endregion
		
#region Common ways to interact with the connections
		/*
		protected void ConnectNewServer(ConnectionInfo ci)
		{
			MainControlFactory.MainControl.ConnectNewServer(ci);
		}
		
		protected void JoinNewChan(string serverName, string chanName)
		{
			MainControlFactory.MainControl.GetConnectionControls(serverName).JoinChannel(chanName);
		}
		
		protected void SendMessageToChannel(string serverName, string channelName, string message)
		{
			MainControlFactory.MainControl.GetConnectionControls(serverName)
				.GetChannelControl(channelName).SendMessage(message);
		}
		
		protected string[] ChannelOpened {
			get {
				return MainControlFactory.MainControl.
			}
		}
		
		protected event EventHandler NewChannelJoined;
		protected event EventHandler NewServerJoined;
		*/
		// In fact plugins worker can use GuiFactory and MainControlFactory now
#endregion
	}
}
