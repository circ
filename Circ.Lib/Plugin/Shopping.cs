#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Controller;

namespace Circ.Plugins
{
	public static class Shop
	{
		static IMainControl mainCtrl;
		
		public static IMainControl MainControl {
			get {
				return mainCtrl;
			}
			internal set {
				mainCtrl = value;
			}
		}
	}
}
