#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;

namespace Circ.Controller
{
	
	public static class MainControlFactory
	{
		static IMainControl mainCtrl;
		
		public static IMainControl MainControl {
			get {
				return mainCtrl;
			}
			set {
				if (value == null)
					throw new ArgumentNullException("value");
				
				mainCtrl.Dispose();
			}
		}
	}
}
