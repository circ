#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;
using Circ.Frontend;

namespace Circ.Controller
{
	public interface IMainControl: IDisposable
	{
		IrcConnection ConnectNewServer(Circ.Backend.ConnectionInfo ci);
		void CommandEntered(object ctrl, string command);
		IConnectionControl GetConnectionControl(string serverName);
		
		IFrontend Frontend { get; }
		IBackend  Backend  { get; }
	}
}
