#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;
using Circ.Frontend;

namespace Circ.Controller
{
	public interface IChannelControl
	{
		//void OpUser(string username);
		//void HalfOpUser(string username);
		//void SendMessage(string message);
		//string ChannelName { get; }
		IrcChannel Backend  { get; }
		IChannelPanel Frontend { get; }
		
		void CloseChannel();
	}
}
