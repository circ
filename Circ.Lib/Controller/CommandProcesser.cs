#region License
/* Circ : Main program
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Circ.Backend;

namespace Circ.Controller
{
	public delegate void CommandFollower(object sender, string[] commandArgs);
	
	public class CommandProcesser
	{
		static CommandProcesser instance = new CommandProcesser();
		static IParser parser = DefaultParser.Instance;
		static CommandFollower defaultFollower;
		
		Dictionary<string, List<CommandFollower>> cmds = new Dictionary<string, List<CommandFollower>>();
		
		public static CommandProcesser Instance {
			get {
				return instance;
			}
		}
		
		public static CommandFollower DefaultFollower {
			get {
				return defaultFollower;
			}
			set {
				if (value == null)
					throw new ArgumentNullException("value");
				defaultFollower = value;
			}
		}
		
		private CommandProcesser()
		{
		}
		
		public void RegisterCommand(string prefix, CommandFollower func)
		{
			if (string.IsNullOrEmpty(prefix))
				throw new ArgumentException("prefix is either null or empty", "prefix");
			
			if (!this.cmds.ContainsKey(prefix))
				cmds[prefix] = new List<CommandFollower>();

			cmds[prefix].Add(func);
		}
		
		public void ProcessCommand(object sender, string prefix, string[] args)
		{
			List<CommandFollower> temp;
			if (!cmds.TryGetValue(prefix, out temp) || temp.Count == 0) {
				string[] tempStr = new string[args.Length + 1];
				tempStr[0] = prefix;
				Array.Copy(args, 0, tempStr, 1, args.Length);
				defaultFollower(sender, tempStr);
				return;
			}
			
			if (temp.Count == 1) {
				temp[0](sender, args);
			} else {
				foreach (CommandFollower cmd in temp) {
					cmd(sender, args);
				}
			}
		}
	}
}
