#region License
/* Circ.Lib : main library behind Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;
using Circ.Frontend;

namespace Circ.Controller
{
	public interface IConnectionControl
	{
		IChannelControl GetChannelControl(string channelName);
		void CloseConnection();
		
		IrcConnection Backend { get; }
		IServerPanel  Frontend { get; }
	}
}
