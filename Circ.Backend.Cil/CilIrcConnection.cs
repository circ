#region License
/* Circ.Cil : Circ's Irc Library, main backend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Collections;
using Circ.Backend;

namespace Circ.Cil
{
	public sealed class CilIrcConnection: IrcConnection
	{
		// State info
		List<IrcChannel> channelsJoined = new List<IrcChannel>();
		
		// Config
		//const int DefaultTime = 1000;
		int messageCount = 0;
		bool identified = false;
		
		// Tcpclient
		TcpClient client = new TcpClient();
		
		// Streams access
		NetworkStream ns;
		StreamWriter sw;
		StreamReader sr;
		
		// Threads
		//Timer readTimer = new Timer(DefaultTime);
		Thread readThread;
		volatile int nbTimes = 0;
		
		// Parser
		IParser parser = DefaultParser.Instance;
		
		public override void Connect()
		{
			if (client.Connected)
				return;
			
			// Event helpers
			this.MessageReceived += AutomaticPong;
			this.MessageReceived += Identification;
			
			// Set common option
			client.LingerState = new LingerOption(true, 2);
			
			try {
				OnConnecting();
				client.Connect(Info.Server, Info.Port);
			} catch (Exception ex) {
				client.Close();
				throw new ConnectionException("The connection to the server wasn't successful : "+ex.Message, ex);
			}
			
			ns = client.GetStream();
			sw = new StreamWriter(ns, Info.Charset);
			sr = new StreamReader(ns, Info.Charset);
			
			// Thread initialization
			readThread = new Thread(WorkerRead);
			readThread.IsBackground = true;
			readThread.Name = "Read Thread";
			readThread.Priority = ThreadPriority.BelowNormal;
			readThread.Start();
			
			// First identification pass try
			if (!string.IsNullOrEmpty(Info.Password))
				SendRawMessage(Rfc.Pass(Info.Password));
			SendRawMessage(Rfc.Nick(Info.Pseudo));
			SendRawMessage(Rfc.User(Info.Pseudo, Info.RealName));
			
			// Inform that we are connected. Warning : Connected doesn't mean Identified
			// (i.e. you can't join channels until you are identified)
			OnConnected();
		}
		
		public override void Disconnect()
		{
			if (client == null || (client != null && !client.Connected))
				return;
			// Send quit message
			SendRawMessage(Rfc.Quit("Powered by Monologue"));
			sw.Flush();
		}
		
		protected override void Dispose (bool managedRes)
		{
			/*foreach (IrcChannel chan in JoinedChannels)
				chan.CloseChannel();*/
            // Disconnect the machine
			this.Disconnect();
            
            if (managedRes)
            {
            	try {
	            	if (sw != null)
	            		sw.Dispose();
	            	if (sr != null)
	            		sr.Dispose();
            	} catch {}
            }
            client.Close();
		}

		
		public override void SendRawMessage(string message)
		{
            if (string.IsNullOrEmpty(message))
            	throw new ArgumentNullException("message");
			
			try {
				// Check if the message is correctly finished with a \r\n sequence
				if (message.EndsWith(Rfc.Crlf))
					sw.Write(message);
				else
					sw.WriteLine(message);

				// Flush the data
				sw.Flush();
			} catch {
				// TODO: maybe use an event instead of this
				throw new ConnectionException("Error, the inner socket has been shut down");
			}
			OnMessageSent(new MessageEventArgs(message));
		}
		
		public override IrcChannel JoinChannel(string channelName)
		{
			if (!identified) {
				while (!identified)
					System.Threading.Thread.Sleep(300);
			}
			IrcChannel temp = new CilIrcChannel(channelName, this);
			channelsJoined.Add(temp);
			OnChannelCreated(new ChannelEventArgs(temp));
			return temp;
		}
		
		public override IList<IrcChannel> JoinedChannels {
			get {
				return channelsJoined.AsReadOnly();
			}
		}

#region Properties
		public override string Nick {
			get { return Info.Pseudo; }
			set {
				if (string.IsNullOrEmpty(value))
					return;
				
				Info.Pseudo = value;
				UpdateNickname();
			}
		}
		
		public override string RealName {
			get { return Info.RealName; }
		}

/*		public override string Server {
			get { return Info.Server; }
		}*/
		
		public bool IsIdentified {
			get { return identified; }	
		}
#endregion

#region private helpers methods
		private void UpdateNickname()
		{
			this.SendRawMessage(Rfc.Nick(Info.Pseudo));
		}
		
		private void WorkerRead()
		{
			int sleepTime = 0;
			string temp;
			
			// Main loop
			while (true) {
				if (!client.Connected)
					return;
				
				// Sleep if the channel is idle
				if (Info.UsePassiveMode && client.Available == 0 && identified) {
					if (sleepTime < 5000 && ++nbTimes % 1500 == 0)
						sleepTime += 20;
					if (nbTimes == int.MaxValue) {
						nbTimes = 0;
					}
					if (sleepTime != 0)
						Thread.Sleep(sleepTime);
				} else {
					sleepTime = 0;	
				}
				
				try {
					// Process data
					while ((temp = sr.ReadLine()) != null)
						OnMessageReceived(new MessageEventArgs(temp));
				} catch (IOException ex) {
					try {
						Logger.Error(string.Format("Exception while reading data. Inner Exception : {O} ::: Error code : {1}", ex.InnerException.Message,
		                    ((ex.InnerException is SocketException) ? ((SocketException)ex.InnerException).ErrorCode.ToString() : "No code")), ex);
					} catch {}
				} catch (Exception ex) {
					Logger.Error("Common error while reading", ex);
				}
				// Let other things happens
				Thread.Sleep(500);
			}
		}
		
		// Big hack, but don't know why some server are reluctant to accept my Nick, User and all messages
		// thus if they want the war so be it (I repeatidly send them the connection commands until they accept
		// me)
		private void Identification(object sender, MessageEventArgs e)
		{
			// Some server return an 451 error and other nothing
			if (((parser.RetrieveMessageType(e.Message) == MessageType.Reply && 
					parser.RetrieveNumericalReply(e.Message) == 451)) || ++messageCount == 3) {
				// Resend the connection commands
				if (!string.IsNullOrEmpty(Info.Password))
					SendRawMessage(Rfc.Pass(Info.Password));
				SendRawMessage(Rfc.Nick(Info.Pseudo));
				SendRawMessage(Rfc.User(Info.Pseudo, Info.RealName));
			}
			if (parser.RetrieveMessageType(e.Message) == MessageType.Reply
							&& parser.RetrieveNumericalReply(e.Message) == 1) {
				this.MessageReceived -= Identification;
				identified = true;
				OnIdentified();					
				messageCount = 0;
			}
		}
		
		// An automatic wrapper which call PONG at each PING messages
		private void AutomaticPong(object sender, MessageEventArgs e)
		{
			if (string.IsNullOrEmpty(e.Message))
				return;
			
			if (parser.RetrieveMessageType(e.Message) == MessageType.Action
				&& parser.RetrieveAction(e.Message) == ActionType.Ping) {
				
				SendRawMessage(Rfc.Pong(parser.RetrieveParameters(e.Message)));
			}
		}
#endregion
	}
}
