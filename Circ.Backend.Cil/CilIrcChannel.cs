#region License
/* Circ.Cil : Circ's Irc Library, main backend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Collections.Generic;
using Circ.Backend;

namespace Circ.Cil
{
	public class CilIrcChannel: IrcChannel
	{
		IParser        parser = DefaultParser.Instance;
		// TODO: Maybe replace the List with a Dictionary for better performance since user are mainly manipulated
		// with their nickname string
		List<IrcUser> userList = new List<IrcUser>();
		ChannelMessageBakery bakery;
		
		internal CilIrcChannel(string channelName, IrcConnection conn)
			: base(channelName, conn)
		{
			bakery = new ChannelMessageBakery(this);
			InitBakery();
			conn.MessageReceived += MessageReceivedParsing;
			System.Threading.Thread.Sleep(300);
			conn.SendRawMessage(Rfc.Join(channelName));
		}
		
		void InitBakery()
		{
			// TODO: turn this into static call using the sender arg
			bakery.Message += delegate(IrcChannel sender, string[] args) {
				Logger.Debug("Receiving message from " + sender.Name);
				OnMessageReceived(new ChannelMessageEventArgs(args[0], args[1]));
			};
			
			bakery.NewList += delegate(IrcChannel sender, string[] users) {
				Logger.Debug("Receiving users list from " + sender.Name);
				AddInitialUsers(users);
			};
			
			bakery.Topic += delegate(IrcChannel sender, string topic) {
				OnTopicUpdated(new TopicEventArgs(topic));
			};
			
			bakery.Quit += delegate(IrcChannel sender, string[] temp) {
				Logger.Debug("An user has departed from " + sender.Name);
				IrcUser usertemp = null;
				if (temp[0] == null)
					return;
				if ((usertemp = GetUserByName(temp[0])) == null)
					return;
				this.userList.Remove(usertemp);
				OnUserDisconnected(new UserQuitEventArgs(temp[1] ?? string.Empty, usertemp));
			};
			
			bakery.Part += delegate(IrcChannel sender, string[] temp) {
				Logger.Debug("An user has departed from " + sender.Name);
				IrcUser usertemp = null;
				if (temp[0] == null)
					return;
				if ((usertemp = GetUserByName(temp[0])) == null)
					return;
				this.userList.Remove(usertemp);
				OnUserDisconnected(new UserQuitEventArgs(temp[1] ?? string.Empty, usertemp));
			};
			
			bakery.Join += delegate(IrcChannel sender, string[] infos) {
				// Dont add ourself
				if (infos[0].Equals(this.ParentConnection.Nick, StringComparison.Ordinal))
					return;
				
				bool halfOp = infos[0][0] == Rfc.HalfOpChar;
				bool op     = infos[0][0] == Rfc.OpChar || infos[0][0] == Rfc.NetworkOp;
				
				IrcUser user = new CilIrcUser((halfOp || op) ? infos[0].Substring(1) : infos[0], infos[1],
				                              this, op, halfOp);
				this.userList.Add(user);
				
				OnNewUserConnected(new UserEventArgs(user));
			};
		}
		
		public override void SendMessage (string message)
		{
			ParentConnection.SendRawMessage(Rfc.PrivMsg(Name, message));
		}

		protected override void SendQuitMessage (string byeMessage)
		{
			ParentConnection.SendRawMessage(Rfc.Part(Name, byeMessage));
		}

		
		private void MessageReceivedParsing(object sender, MessageEventArgs e)
		{
			// Now evrything is handled via event
			
			bakery.PostNewMessage(e.Message);
			/*
			bool isAction = parser.RetrieveMessageType(e.Message) == MessageType.Action;
			ActionType action = (isAction) ? parser.RetrieveAction(e.Message) : (ActionType)0;
			
			try {
				if ()
					OnMessageReceived(new ChannelMessageEventArgs(parser.RetrieveAuthor(e.Message),
							                                       parser.RetrieveMessage(e.Message)));
				else if (msgType == MessageType.Reply
							&& parser.RetrieveNumericalReply(e.Message) == Rfc.NamesReply && parser.RetrieveChannelTarget(e.Message) == this.Name)
					AddInitialUsers(parser.RetrieveUsersList(e.Message));
				else if (msgType == MessageType.Action && parser.RetrieveAction(e.Message) == ActionType.Join && parser.RetrieveChannelTarget(e.Message) == this.Name) {
					string[] temp = parser.RetrieveJoinInformations(e.Message);
					// Dont add ourself
					if (temp[0].Equals(this.ParentConnection.Nick, StringComparison.Ordinal))
						return;
					
					IrcUser user = new CilIrcUser(temp[0], temp[1], this, false, temp[0][0] == Rfc.HalfOpChar);
					this.userList.Add(user);
					// TODO: handle op thing correctly
					OnNewUserConnected(new UserEventArgs(user));
				}
				else if (msgType == MessageType.Action && parser.RetrieveAction(e.Message) == ActionType.Part && parser.RetrieveChannelTarget(e.Message) == this.Name) {
					string[] temp = parser.RetrieveQuitInformations(e.Message);
					IrcUser usertemp = null;
					if (temp[0] == null)
						return;
					if ((usertemp = GetUserByName(temp[0])) == null)
						return;
					this.userList.Remove(usertemp);
					OnUserDisconnected(new UserQuitEventArgs(temp[1] ?? string.Empty, usertemp));
				}
				else if (msgType == MessageType.Reply
				            && parser.RetrieveNumericalReply(e.Message) == Rfc.TopicReply && parser.RetrieveChannelTarget(e.Message) == this.Name)
					OnTopicUpdated(new TopicEventArgs(parser.RetrieveTopic(e.Message)));
			} catch (Exception ex) {
				Logger.Error("A message has been dropped", ex);
			}*/
		}
		
#region worker methods
		
		
#endregion
		
#region Helper methods
		private void AddInitialUsers(string[] names)
		{
			if (names == null)
				throw new ArgumentNullException("names");
			if (names.Length == 0)
				throw new ArgumentException("names is empty");
			
			userList.Clear();
			
			// HACK: WTF ? IndexOutOfRange even with names.Length
			for (int i = 0; i< names.Length - 1; i++) {
				if (names[i][0] == Rfc.HalfOpChar)
					userList.Add(new CilIrcUser(names[i].Substring(1), "foo", this, false, true));
				else if (names[i][0] == Rfc.OpChar)
					userList.Add(new CilIrcUser(names[i].Substring(1), "foo", this, true, false));
				else
					userList.Add(new CilIrcUser(names[i], "foo", this, false, false));
			}
			
			IrcUser[] temp = userList.ToArray();
			
			OnNewUserList(new UserListEventArgs(userList.ToArray()));
		}
		
		private IrcUser GetUserByName(string userName)
		{
			IrcUser temp = null;
			foreach (IrcUser user in userList) {
				if (user.Nick == userName) {
					temp = user;
					break;
				}
			}
			return temp;
		}
#endregion
	}
}
