#region License
/* Circ.Cil : Circ's Irc Library, main backend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using Circ.Backend;

namespace Circ.Cil
{
	
	[Mono.Addins.Extension("/Circ/Backends", Id="Cil")]
	public class CilBackend: IBackend
	{
		public ConnectionFactory GetConnectionFactory()
		{
			return new ConnectionFactory<CilIrcConnection>();	
		}
		
		public string Name {
			get {
				return "Cil";
			}
		}
	}
}
