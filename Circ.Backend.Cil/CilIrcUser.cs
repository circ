#region License
/* Circ.Cil : Circ's Irc Library, main backend for Circ
 * Copyright (C) 2007  LAVAL Jérémie
 *
 * This file is licensed under the terms of the LGPL.
 *
 * For the complete licence see the file COPYING.
 */
#endregion
using System;
using System.Net;
using System.Net.Sockets;
using Circ.Backend;

namespace Circ.Cil
{
	public class CilIrcUser: IrcUser
	{
		bool         op;
		bool         halfOp;
		
		// TODO: refractoring : use an enum to describe op/half-op states
		internal CilIrcUser(string pseudo, string realName, IrcChannel parentChannel, bool op, bool halfOp)
			: base(pseudo, realName, parentChannel)
		{
            this.op = op;
            this.halfOp = halfOp;
		}
		
		public override bool IsOp {
			get {
				return op;
			}
			set {
				throw new NotImplementedException();
			}
		}
		
		public override bool IsHalfOp {
			get {
				return halfOp;
			}
			set {
				throw new NotImplementedException();
			}
		}
		
		//public override void Voice() {}
		//public override void Ban() {}
		//public override void Kick() {}
		public override void PrivMessage (string message)
		{
			ParentChannel.ParentConnection.SendRawMessage(Rfc.PrivMsg(this.Nick, message));
		}

	}
}
