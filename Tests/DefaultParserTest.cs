using System;
using Circ.Lib;
using Circ.Backend;
using NUnit.Core;
using NUnit.Framework;

[TestFixtureAttribute]
public partial class DefaultParserTest
{
    
    public DefaultParserTest()
    {
    }
    
	IParser parser;
	
	[SetUp]
	public void Setup()
	{
		parser = new DefaultParser();
	}
	
	[Test]
	public void TestPingPong()
	{
		string ping = "PING :B4C785BC";
		Assert.IsTrue(parser.RetrieveMessageType(ping) == MessageType.Action, "#1");
		Assert.IsTrue(parser.RetrieveAction(ping) == ActionType.Ping, "#2");
		Assert.IsTrue(parser.RetrieveParameters(ping) == ":B4C785BC", "#3");
		Assert.IsTrue(Rfc.Pong(parser.RetrieveParameters(ping)) == "PONG :B4C785BC"+Rfc.Crlf, "#4");
	}
	
	[Test]
	public void BadIndentification()
	{
		string badIdent = ":trucnuche.epikne.org 451 :You have not registered";
		string other = ":trucnuche.epikne.org 453 :Some other error";
		
		Assert.IsTrue(((parser.RetrieveMessageType(badIdent) == MessageType.Reply && 
		                   parser.RetrieveNumericalReply(badIdent) == 451)), "#1");
		Assert.IsFalse(((parser.RetrieveMessageType(other) == MessageType.Reply && 
		                   parser.RetrieveNumericalReply(other) == 451)), "#2");
	}
	
	[Test]
	public void TestNoticeAuth()
	{
		string auth = ":la-defense2.fr.epiknet.org NOTICE AUTH :*** Found your hostname";
		string nonAuth = ":Themis!services@olympe.epiknet.org NOTICE Garuma :Si vous ne changez pas d'ici 1 minute, je changerai votre pseudo.";
		
		Assert.IsTrue(parser.RetrieveMessageType(auth) == MessageType.Notice, "#1");
		Assert.IsTrue(parser.RetrieveParameters(auth).Substring(0, 4).ToUpperInvariant() == "AUTH", "#2");
		
		Assert.IsTrue(parser.RetrieveMessageType(nonAuth) == MessageType.Notice, "#3");
		Assert.IsTrue(parser.RetrieveParameters(nonAuth).Substring(0, 4).ToUpperInvariant() != "AUTH", "#4");
	}
	
	[Test]
	public void TestUserList()
	{
		string userList = ":la-defense2.fr.epiknet.org 353 Garuma = #sdz-unix :Garuma @Terpsichore GarulfoLinux";
		
		Assert.IsTrue(parser.RetrieveMessageType(userList) == MessageType.Reply, "#1");
		Assert.IsTrue(parser.RetrieveNumericalReply(userList) == Rfc.NamesReply, "#2");
		Assert.IsTrue(string.Join(" ", parser.RetrieveUsersList(userList)) == "Garuma @Terpsichore GarulfoLinux", "#3");
	}
	
	[Test]
	public void TestPrivateMessageChannel()
	{
		string cmd = ":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net PRIVMSG #sdz-unix :Anonyme942099: haha couillon a deux balle";
		Assert.IsTrue(parser.RetrieveMessageType(cmd) == MessageType.PrivateMessage, "#1");
		Assert.IsTrue(parser.RetrieveMessageTarget(cmd) == "#sdz-unix", "#2");
		Assert.IsTrue(parser.RetrieveMessage(cmd) == "Anonyme942099: haha couillon a deux balle", "#3 : Message is "+parser.RetrieveMessage(cmd));
	}
	
	[Test]
	public void RetrieveJoinInformationsTest()
	{
		string join = ":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net JOIN :#sdz-unix";
		
		Assert.IsTrue(parser.RetrieveMessageType(join) == MessageType.Action, "MessageType");
		Assert.IsTrue(parser.RetrieveAction(join) == ActionType.Join, "ActionType");
		Assert.IsTrue(parser.RetrieveChannelTarget(join) == "#sdz-unix", "ChannelTarget");
		
		string[] temp = parser.RetrieveJoinInformations(join);
		Assert.IsNotNull(temp, "NotNullInformations");
		Assert.IsNotEmpty(temp);
		Assert.IsTrue(temp[0].Equals("Garuma_", System.StringComparison.Ordinal), "NickEquality : " + temp[0]);
		Assert.IsTrue(temp[1].Equals("~garuma", System.StringComparison.Ordinal), "UsernameEquality : " + temp[1]);
	}
	
	[Test]
	public void RetrieveQuitInformationsTest()
	{
		string cmd = ":Garuma!~garuma@EpiK-B8FEBE74.fbx.proxad.net QUIT :Client exited";
		
		Assert.IsTrue(parser.RetrieveMessageType(cmd) == MessageType.Action, "MessageType");
		Assert.IsTrue(parser.RetrieveAction(cmd) == ActionType.Quit, "ActionType");
		
		string[] temp = parser.RetrieveQuitInformations(cmd);
		Assert.IsNotNull(temp, "NotNullInformations");
		Assert.IsNotEmpty(temp);
		Assert.IsTrue(temp[0].Equals("Garuma", System.StringComparison.Ordinal), "NickEquality : " + temp[0]);
		Assert.IsTrue(temp[1].Equals("Client exited", System.StringComparison.Ordinal), "QuitMessageEquality : " + temp[1]);
	}
	
	[Test]
	public void RetrievePartInformationsTest()
	{
		string cmd = ":Garuma!~garuma@EpiK-B8FEBE74.fbx.proxad.net PART #sdz-unix :Client exited";
		
		Assert.IsTrue(parser.RetrieveMessageType(cmd) == MessageType.Action, "MessageType");
		Assert.IsTrue(parser.RetrieveAction(cmd) == ActionType.Part, "ActionType");
		Assert.IsTrue(parser.RetrieveChannelTarget(cmd) == "#sdz-unix", "ChannelTarget : " + parser.RetrieveChannelTarget(cmd));
		
		string[] temp = parser.RetrieveQuitInformations(cmd);
		Assert.IsNotNull(temp, "NotNullInformations");
		Assert.IsNotEmpty(temp);
		Assert.IsTrue(temp[0].Equals("Garuma", System.StringComparison.Ordinal), "NickEquality : " + temp[0]);
		Assert.IsTrue(temp[1].Equals("Client exited", System.StringComparison.Ordinal), "QuitMessageEquality : " + temp[1]);
	}
		
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveAuthorTest()
    {
        // TODO: Implement unit test for RetrieveAuthorTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveMessageTest()
    {
        // TODO: Implement unit test for RetrieveMessageTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveMessageTargetTest()
    {
        // TODO: Implement unit test for RetrieveMessageTargetTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveMessageTypeTest()
    {
        // TODO: Implement unit test for RetrieveMessageTypeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveActionTest()
    {
        // TODO: Implement unit test for RetrieveActionTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveParametersTest()
    {
        // TODO: Implement unit test for RetrieveParametersTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveNoticeTextTest()
    {
        // TODO: Implement unit test for RetrieveNoticeTextTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveNumericalReplyTest()
    {
        // TODO: Implement unit test for RetrieveNumericalReplyTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveUsersListTest()
    {
        // TODO: Implement unit test for RetrieveUsersListTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void RetrieveTopicTest()
    {
        // TODO: Implement unit test for RetrieveTopicTest
    }
    
    [NUnit.Framework.TestAttribute()]
    public void RetrieveChannelTargetTest()
    {
        string cmd = ":la-defense2.fr.epiknet.org 332 Garuma #sdz-unix :Canal IRC Francophone dédié à l'utilisation/au parlage de systèmes Unix --- Salut à toi, et mort aux cons : http://chl.be/vista/";
		string cmd2 = ":la-defense2.fr.epiknet.org 353 Garuma = #sdz-unix :Garuma @Terpsichore GarulfoLinux ";
		string cmd3 = ":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net JOIN :#sdz-unix";
		
		string result = null;
		
		Assert.IsNotNull((result = parser.RetrieveChannelTarget(cmd)));
		Assert.IsTrue(result.Equals("#sdz-unix", StringComparison.Ordinal), "Equality : " + result);
		
		Assert.IsNotNull((result = parser.RetrieveChannelTarget(cmd2)));
		Assert.IsTrue(result.Equals("#sdz-unix", StringComparison.Ordinal), "Equality 2 : " + result);
		
		Assert.IsNotNull((result = parser.RetrieveChannelTarget(cmd3)));
		Assert.IsTrue(result.Equals("#sdz-unix", StringComparison.Ordinal), "Equality 3 : " + result);
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void StripFirstColonTest()
    {
        // TODO: Implement unit test for StripFirstColonTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void EqualsTest()
    {
        // TODO: Implement unit test for EqualsTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetHashCodeTest()
    {
        // TODO: Implement unit test for GetHashCodeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetTypeTest()
    {
        // TODO: Implement unit test for GetTypeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void ToStringTest()
    {
        // TODO: Implement unit test for ToStringTest
    }
}
