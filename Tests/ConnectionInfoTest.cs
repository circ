using NUnit.Framework;
using Circ.Backend;

[NUnit.Framework.TestFixtureAttribute]
public partial class ConnectionInfoTest
{
    
    public ConnectionInfoTest()
    {
    }
	
	ConnectionInfo ci;
	
	[SetUpAttribute]
	void Setup()
	{
		ci = new Circ.Backend.ConnectionInfo("irc.foo.org", 6667, "Foo", "Bar", "FooBar");
	}
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_ServerTest()
    {
        Assert.IsNotNull(ci.Server, "Nullity");
		Assert.IsTrue(ci.Server == "irc.foo.org", "Server equality");
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_PortTest()
    {
        // TODO: Implement unit test for get_PortTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_PseudoTest()
    {
        // TODO: Implement unit test for get_PseudoTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void set_PseudoTest()
    {
        // TODO: Implement unit test for set_PseudoTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_PasswordTest()
    {
        // TODO: Implement unit test for get_PasswordTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_RealNameTest()
    {
        // TODO: Implement unit test for get_RealNameTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_CharsetTest()
    {
        // TODO: Implement unit test for get_CharsetTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void set_CharsetTest()
    {
        // TODO: Implement unit test for set_CharsetTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_UseSslTest()
    {
        // TODO: Implement unit test for get_UseSslTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void set_UseSslTest()
    {
        // TODO: Implement unit test for set_UseSslTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_UsePassiveModeTest()
    {
        // TODO: Implement unit test for get_UsePassiveModeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void set_UsePassiveModeTest()
    {
        // TODO: Implement unit test for set_UsePassiveModeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void get_QuitMessageTest()
    {
        // TODO: Implement unit test for get_QuitMessageTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void set_QuitMessageTest()
    {
        // TODO: Implement unit test for set_QuitMessageTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void EqualsTest()
    {
        // TODO: Implement unit test for EqualsTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetHashCodeTest()
    {
        // TODO: Implement unit test for GetHashCodeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetTypeTest()
    {
        // TODO: Implement unit test for GetTypeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void ToStringTest()
    {
        // TODO: Implement unit test for ToStringTest
    }
}
