// Common.cs created with MonoDevelop
// User: jeremie at 19:18 22/07/2007
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers


using System;
using Circ.Backend;
using System.Collections.Generic;

internal class TestIrcConn : IrcConnection
{
	public event EventHandler ConnectCalled;
	public event EventHandler DisconnectCalled;
	public event EventHandler JoinChannelCalled;
	public event EventHandler DisposeCalled;
	public event EventHandler SendRawMessageCalled;
	
	public TestIrcConn(): base()
	{
	}

	public override void Connect ()
	{
		
	}
	
	public override void Disconnect ()
	{
		
	}
			
	public override IrcChannel JoinChannel (string channelName)
	{
		return null;
	}
	
	public override IList<IrcChannel> JoinedChannels {
		get { return null; }
	}

	public override string Nick {
		get { return "Foo"; }
		set {  }
	}

	protected override void Dispose (bool managedRes)
	{
		
	}
	
	public override string RealName {
		get { return string.Empty; }
	}

	public override void SendRawMessage (string message)
	{
		
	}
}

internal class TestIrcChan: IrcChannel
{
	public TestIrcChan(string chan, IrcConnection parent): base(chan, parent)
	{
		
	}
	
	protected override void SendQuitMessage(string byeMessage)
	{
		
	}

	public override void SendMessage (string message)
	{
		
	}

}

		