using NUnit.Framework;
using Circ.Backend;

[NUnit.Framework.TestFixtureAttribute]
public partial class RfcTest
{
    
    public RfcTest()
    {
    }
    
    [NUnit.Framework.TestAttribute()]
    public void IsMessageTooLongTest()
    {
		Assert.IsTrue(Rfc.IsMessageTooLong(new string('c', 560)), "#1");
    }
    
    [NUnit.Framework.TestAttribute()]
    public void AdaptMessageSizeTest()
    {
        string messageToLong = @":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net PRIVMSG #sdz-unix :Anonyme942099: This message is far toolonglalalalalalalalalalalalalalalalalalalalalalalalalaaaaaaaa:aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		string[] result = Rfc.AdaptMessageSize(messageToLong);
		Assert.IsNotNull(result);
		Assert.IsNotEmpty(result);
		Assert.IsTrue(result.Length == 2, "Array length, should be equal to 2");
		Assert.IsTrue(result[0].StartsWith(":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net PRIVMSG #sdz-unix :") &&
			              result[1].StartsWith(":Garuma_!~garuma@EpiK-B8FEBE74.fbx.proxad.net PRIVMSG #sdz-unix :"), "Check the beginning of each message");
		Assert.IsFalse(Rfc.IsMessageTooLong(result[0]) || Rfc.IsMessageTooLong(result[1]));
    }
    
    [NUnit.Framework.TestAttribute()]
    public void PassTest()
    {
        string result = Rfc.Pass("Foo");
		Assert.IsNotNull(result);
		Assert.IsNotEmpty(result);
		Assert.IsTrue(result.StartsWith("PASS Foo", System.StringComparison.Ordinal));
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void NickTest()
    {
        // TODO: Implement unit test for NickTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void UserTest()
    {
        // TODO: Implement unit test for UserTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void QuitTest()
    {
        // TODO: Implement unit test for QuitTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void PongTest()
    {
        // TODO: Implement unit test for PongTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void JoinTest()
    {
        // TODO: Implement unit test for JoinTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void PartTest()
    {
        // TODO: Implement unit test for PartTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void PrivMsgTest()
    {
        // TODO: Implement unit test for PrivMsgTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void EqualsTest()
    {
        // TODO: Implement unit test for EqualsTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetHashCodeTest()
    {
        // TODO: Implement unit test for GetHashCodeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void GetTypeTest()
    {
        // TODO: Implement unit test for GetTypeTest
    }
    
    [NUnit.Framework.TestAttribute()]
    [NUnit.Framework.IgnoreAttribute()]
    void ToStringTest()
    {
        // TODO: Implement unit test for ToStringTest
    }
}
